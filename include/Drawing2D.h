/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#ifndef __DRAWING2D__H__
#define __DRAWING2D__H__
#include <inttypes.h>
#include <stdlib.h> 

namespace Drawing2D {
	struct Image {
		void *data;
		int width, height, stride;
		bool ok;
	};

	struct Font {
		void *data;
		uint32_t ptsize;
		Image atlas;
	};

	typedef uint16_t Color;
	
	Color RGBA8(
		uint8_t red,
		uint8_t green,
		uint8_t blue,
		uint8_t alpha
	);

	const Color COL_WHITE = RGBA8(255,255,255,255);
	const Color COL_BLACK = RGBA8(0,0,0,255);

	Image loadImage(
		const char *path
	);

	Font loadFont(
		const char *path,
		int ptsize
	);
	
	void freeImage(Image &img);
	void freeFont(Font &font);

	void InitDrawing(uint8_t *vertbuf, size_t vertbuf_len);
	void BeginDrawing();
	void EndDrawing(bool reset_verts = false);

	void DrawRectangle(
		int x,
		int y,
		int w,
		int h,
		Color color
	);

	void DrawText(
		const Font &font,
		const char *text,
		float x,
		float y,
		Color color = COL_WHITE,
		float scale_x = 1.0,
		float scale_y = 1.0
	);

	void DrawLine(
		int x0,
		int y0,
		int x1,
		int y1,
		Color color
	);

	void SetImageFilter(bool linear);

	void DrawImage(
		const Image &tex,
		float x,
		float y,
		float tex_x = 0.0,
		float tex_y = 0.0,
		float tex_w = 1.0,
		float tex_h = 1.0,
		float x_scale = 1.0,
		float y_scale = 1.0,
		float angle = 0.0,
		Color tint = COL_WHITE
	);
};
#endif //__DRAWING2D__H__