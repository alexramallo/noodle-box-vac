/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#ifndef __GAME__H__
#define __GAME__H__
#include <pspctrl.h>
#include <pspgum.h>
#include "Scene.h"
#include "PSPMesh_generated.h"
#include "PSPScene_generated.h"

#define FB_START ((void*)0)
#define SCR_WIDTH (480)
#define SCR_HEIGHT (272)
#define BUF_WIDTH (512)

#define FB_32 false

#if FB_32
	//32 bit framebuffer mode
	#define BUF_SIZE (BUF_WIDTH * SCR_HEIGHT * 4)
	#define DRAW_FMT GU_PSM_8888
#else
	//16 bit framebuffer mode
	#define BUF_SIZE (BUF_WIDTH * SCR_HEIGHT * 2)
	#define DRAW_FMT GU_PSM_5650
#endif

#define pDraw 		((void*)(FB_START))
#define pDisplay 	((void*)(FB_START + (BUF_SIZE * 1) ))
#define pDepth 		((void*)(FB_START + (BUF_SIZE * 2) ))

#define DisplayList(X) unsigned int __attribute__((aligned(16))) X;

class Game {
public:
	int boot(int argc, char *argv[]);
	void tick(float elapsed);
	void do_exit();
	int init_display();

	static void drawMesh(const Engine::Graphics::PSPMesh *mesh);

public:
	Scene *scWarehouse;
	SceCtrlData pad;

	ScePspFVector3 cam_rot;
	ScePspFVector3 cam_pos;
	ScePspFMatrix4 cam_mat;

	DisplayList(dlist_main[48 * 1024]);
};

#endif //__GAME__H__