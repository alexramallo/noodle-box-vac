/*
  Copyright (c) 2022 Alejandro Ramallo

  This file is part of VAC Demo
  Licensed under the BSD license, see the file "COPYING" for details.
*/
// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_PSPSCENE_ENGINE_GRAPHICS_H_
#define FLATBUFFERS_GENERATED_PSPSCENE_ENGINE_GRAPHICS_H_

#include "flatbuffers/flatbuffers.h"

namespace Engine {
namespace Graphics {

struct Vec3;

struct Transform;

struct PSPSceneNode;
struct PSPSceneNodeBuilder;

struct PSPMeshNode;
struct PSPMeshNodeBuilder;

struct PSPCameraNode;
struct PSPCameraNodeBuilder;

struct PSPScene;
struct PSPSceneBuilder;

FLATBUFFERS_MANUALLY_ALIGNED_STRUCT(4) Vec3 FLATBUFFERS_FINAL_CLASS {
 private:
  float x_;
  float y_;
  float z_;

 public:
  Vec3()
      : x_(0),
        y_(0),
        z_(0) {
  }
  Vec3(float _x, float _y, float _z)
      : x_(flatbuffers::EndianScalar(_x)),
        y_(flatbuffers::EndianScalar(_y)),
        z_(flatbuffers::EndianScalar(_z)) {
  }
  float x() const {
    return flatbuffers::EndianScalar(x_);
  }
  float y() const {
    return flatbuffers::EndianScalar(y_);
  }
  float z() const {
    return flatbuffers::EndianScalar(z_);
  }
};
FLATBUFFERS_STRUCT_END(Vec3, 12);

FLATBUFFERS_MANUALLY_ALIGNED_STRUCT(4) Transform FLATBUFFERS_FINAL_CLASS {
 private:
  Engine::Graphics::Vec3 translation_;
  Engine::Graphics::Vec3 rotation_;
  Engine::Graphics::Vec3 scale_;

 public:
  Transform()
      : translation_(),
        rotation_(),
        scale_() {
  }
  Transform(const Engine::Graphics::Vec3 &_translation, const Engine::Graphics::Vec3 &_rotation, const Engine::Graphics::Vec3 &_scale)
      : translation_(_translation),
        rotation_(_rotation),
        scale_(_scale) {
  }
  const Engine::Graphics::Vec3 &translation() const {
    return translation_;
  }
  const Engine::Graphics::Vec3 &rotation() const {
    return rotation_;
  }
  const Engine::Graphics::Vec3 &scale() const {
    return scale_;
  }
};
FLATBUFFERS_STRUCT_END(Transform, 36);

struct PSPSceneNode FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef PSPSceneNodeBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_NAME = 4,
    VT_TRANS = 6
  };
  const flatbuffers::String *name() const {
    return GetPointer<const flatbuffers::String *>(VT_NAME);
  }
  const Engine::Graphics::Transform *trans() const {
    return GetStruct<const Engine::Graphics::Transform *>(VT_TRANS);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_NAME) &&
           verifier.VerifyString(name()) &&
           VerifyField<Engine::Graphics::Transform>(verifier, VT_TRANS) &&
           verifier.EndTable();
  }
};

struct PSPSceneNodeBuilder {
  typedef PSPSceneNode Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_name(flatbuffers::Offset<flatbuffers::String> name) {
    fbb_.AddOffset(PSPSceneNode::VT_NAME, name);
  }
  void add_trans(const Engine::Graphics::Transform *trans) {
    fbb_.AddStruct(PSPSceneNode::VT_TRANS, trans);
  }
  explicit PSPSceneNodeBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<PSPSceneNode> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<PSPSceneNode>(end);
    return o;
  }
};

inline flatbuffers::Offset<PSPSceneNode> CreatePSPSceneNode(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> name = 0,
    const Engine::Graphics::Transform *trans = 0) {
  PSPSceneNodeBuilder builder_(_fbb);
  builder_.add_trans(trans);
  builder_.add_name(name);
  return builder_.Finish();
}

inline flatbuffers::Offset<PSPSceneNode> CreatePSPSceneNodeDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *name = nullptr,
    const Engine::Graphics::Transform *trans = 0) {
  auto name__ = name ? _fbb.CreateString(name) : 0;
  return Engine::Graphics::CreatePSPSceneNode(
      _fbb,
      name__,
      trans);
}

struct PSPMeshNode FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef PSPMeshNodeBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_NAME = 4,
    VT_TRANS = 6,
    VT_MESH = 8
  };
  const flatbuffers::String *name() const {
    return GetPointer<const flatbuffers::String *>(VT_NAME);
  }
  const Engine::Graphics::Transform *trans() const {
    return GetStruct<const Engine::Graphics::Transform *>(VT_TRANS);
  }
  int32_t mesh() const {
    return GetField<int32_t>(VT_MESH, 0);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_NAME) &&
           verifier.VerifyString(name()) &&
           VerifyField<Engine::Graphics::Transform>(verifier, VT_TRANS) &&
           VerifyField<int32_t>(verifier, VT_MESH) &&
           verifier.EndTable();
  }
};

struct PSPMeshNodeBuilder {
  typedef PSPMeshNode Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_name(flatbuffers::Offset<flatbuffers::String> name) {
    fbb_.AddOffset(PSPMeshNode::VT_NAME, name);
  }
  void add_trans(const Engine::Graphics::Transform *trans) {
    fbb_.AddStruct(PSPMeshNode::VT_TRANS, trans);
  }
  void add_mesh(int32_t mesh) {
    fbb_.AddElement<int32_t>(PSPMeshNode::VT_MESH, mesh, 0);
  }
  explicit PSPMeshNodeBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<PSPMeshNode> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<PSPMeshNode>(end);
    return o;
  }
};

inline flatbuffers::Offset<PSPMeshNode> CreatePSPMeshNode(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> name = 0,
    const Engine::Graphics::Transform *trans = 0,
    int32_t mesh = 0) {
  PSPMeshNodeBuilder builder_(_fbb);
  builder_.add_mesh(mesh);
  builder_.add_trans(trans);
  builder_.add_name(name);
  return builder_.Finish();
}

inline flatbuffers::Offset<PSPMeshNode> CreatePSPMeshNodeDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *name = nullptr,
    const Engine::Graphics::Transform *trans = 0,
    int32_t mesh = 0) {
  auto name__ = name ? _fbb.CreateString(name) : 0;
  return Engine::Graphics::CreatePSPMeshNode(
      _fbb,
      name__,
      trans,
      mesh);
}

struct PSPCameraNode FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef PSPCameraNodeBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_NAME = 4,
    VT_TRANS = 6,
    VT_ASPECT_RATIO = 8,
    VT_YFOV = 10,
    VT_ZFAR = 12,
    VT_ZNEAR = 14
  };
  const flatbuffers::String *name() const {
    return GetPointer<const flatbuffers::String *>(VT_NAME);
  }
  const Engine::Graphics::Transform *trans() const {
    return GetStruct<const Engine::Graphics::Transform *>(VT_TRANS);
  }
  float aspect_ratio() const {
    return GetField<float>(VT_ASPECT_RATIO, 0.0f);
  }
  float yfov() const {
    return GetField<float>(VT_YFOV, 0.0f);
  }
  float zfar() const {
    return GetField<float>(VT_ZFAR, 0.0f);
  }
  float znear() const {
    return GetField<float>(VT_ZNEAR, 0.0f);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_NAME) &&
           verifier.VerifyString(name()) &&
           VerifyField<Engine::Graphics::Transform>(verifier, VT_TRANS) &&
           VerifyField<float>(verifier, VT_ASPECT_RATIO) &&
           VerifyField<float>(verifier, VT_YFOV) &&
           VerifyField<float>(verifier, VT_ZFAR) &&
           VerifyField<float>(verifier, VT_ZNEAR) &&
           verifier.EndTable();
  }
};

struct PSPCameraNodeBuilder {
  typedef PSPCameraNode Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_name(flatbuffers::Offset<flatbuffers::String> name) {
    fbb_.AddOffset(PSPCameraNode::VT_NAME, name);
  }
  void add_trans(const Engine::Graphics::Transform *trans) {
    fbb_.AddStruct(PSPCameraNode::VT_TRANS, trans);
  }
  void add_aspect_ratio(float aspect_ratio) {
    fbb_.AddElement<float>(PSPCameraNode::VT_ASPECT_RATIO, aspect_ratio, 0.0f);
  }
  void add_yfov(float yfov) {
    fbb_.AddElement<float>(PSPCameraNode::VT_YFOV, yfov, 0.0f);
  }
  void add_zfar(float zfar) {
    fbb_.AddElement<float>(PSPCameraNode::VT_ZFAR, zfar, 0.0f);
  }
  void add_znear(float znear) {
    fbb_.AddElement<float>(PSPCameraNode::VT_ZNEAR, znear, 0.0f);
  }
  explicit PSPCameraNodeBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<PSPCameraNode> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<PSPCameraNode>(end);
    return o;
  }
};

inline flatbuffers::Offset<PSPCameraNode> CreatePSPCameraNode(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> name = 0,
    const Engine::Graphics::Transform *trans = 0,
    float aspect_ratio = 0.0f,
    float yfov = 0.0f,
    float zfar = 0.0f,
    float znear = 0.0f) {
  PSPCameraNodeBuilder builder_(_fbb);
  builder_.add_znear(znear);
  builder_.add_zfar(zfar);
  builder_.add_yfov(yfov);
  builder_.add_aspect_ratio(aspect_ratio);
  builder_.add_trans(trans);
  builder_.add_name(name);
  return builder_.Finish();
}

inline flatbuffers::Offset<PSPCameraNode> CreatePSPCameraNodeDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *name = nullptr,
    const Engine::Graphics::Transform *trans = 0,
    float aspect_ratio = 0.0f,
    float yfov = 0.0f,
    float zfar = 0.0f,
    float znear = 0.0f) {
  auto name__ = name ? _fbb.CreateString(name) : 0;
  return Engine::Graphics::CreatePSPCameraNode(
      _fbb,
      name__,
      trans,
      aspect_ratio,
      yfov,
      zfar,
      znear);
}

struct PSPScene FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef PSPSceneBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_NAME = 4,
    VT_EMPTY_NODES = 6,
    VT_MESH_NODES = 8,
    VT_CAMERAS = 10,
    VT_MESHES = 12
  };
  const flatbuffers::String *name() const {
    return GetPointer<const flatbuffers::String *>(VT_NAME);
  }
  const flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPSceneNode>> *empty_nodes() const {
    return GetPointer<const flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPSceneNode>> *>(VT_EMPTY_NODES);
  }
  const flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPMeshNode>> *mesh_nodes() const {
    return GetPointer<const flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPMeshNode>> *>(VT_MESH_NODES);
  }
  const flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPCameraNode>> *cameras() const {
    return GetPointer<const flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPCameraNode>> *>(VT_CAMERAS);
  }
  const flatbuffers::Vector<flatbuffers::Offset<flatbuffers::String>> *meshes() const {
    return GetPointer<const flatbuffers::Vector<flatbuffers::Offset<flatbuffers::String>> *>(VT_MESHES);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_NAME) &&
           verifier.VerifyString(name()) &&
           VerifyOffset(verifier, VT_EMPTY_NODES) &&
           verifier.VerifyVector(empty_nodes()) &&
           verifier.VerifyVectorOfTables(empty_nodes()) &&
           VerifyOffset(verifier, VT_MESH_NODES) &&
           verifier.VerifyVector(mesh_nodes()) &&
           verifier.VerifyVectorOfTables(mesh_nodes()) &&
           VerifyOffset(verifier, VT_CAMERAS) &&
           verifier.VerifyVector(cameras()) &&
           verifier.VerifyVectorOfTables(cameras()) &&
           VerifyOffset(verifier, VT_MESHES) &&
           verifier.VerifyVector(meshes()) &&
           verifier.VerifyVectorOfStrings(meshes()) &&
           verifier.EndTable();
  }
};

struct PSPSceneBuilder {
  typedef PSPScene Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_name(flatbuffers::Offset<flatbuffers::String> name) {
    fbb_.AddOffset(PSPScene::VT_NAME, name);
  }
  void add_empty_nodes(flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPSceneNode>>> empty_nodes) {
    fbb_.AddOffset(PSPScene::VT_EMPTY_NODES, empty_nodes);
  }
  void add_mesh_nodes(flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPMeshNode>>> mesh_nodes) {
    fbb_.AddOffset(PSPScene::VT_MESH_NODES, mesh_nodes);
  }
  void add_cameras(flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPCameraNode>>> cameras) {
    fbb_.AddOffset(PSPScene::VT_CAMERAS, cameras);
  }
  void add_meshes(flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<flatbuffers::String>>> meshes) {
    fbb_.AddOffset(PSPScene::VT_MESHES, meshes);
  }
  explicit PSPSceneBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<PSPScene> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<PSPScene>(end);
    return o;
  }
};

inline flatbuffers::Offset<PSPScene> CreatePSPScene(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::String> name = 0,
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPSceneNode>>> empty_nodes = 0,
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPMeshNode>>> mesh_nodes = 0,
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Engine::Graphics::PSPCameraNode>>> cameras = 0,
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<flatbuffers::String>>> meshes = 0) {
  PSPSceneBuilder builder_(_fbb);
  builder_.add_meshes(meshes);
  builder_.add_cameras(cameras);
  builder_.add_mesh_nodes(mesh_nodes);
  builder_.add_empty_nodes(empty_nodes);
  builder_.add_name(name);
  return builder_.Finish();
}

inline flatbuffers::Offset<PSPScene> CreatePSPSceneDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const char *name = nullptr,
    const std::vector<flatbuffers::Offset<Engine::Graphics::PSPSceneNode>> *empty_nodes = nullptr,
    const std::vector<flatbuffers::Offset<Engine::Graphics::PSPMeshNode>> *mesh_nodes = nullptr,
    const std::vector<flatbuffers::Offset<Engine::Graphics::PSPCameraNode>> *cameras = nullptr,
    const std::vector<flatbuffers::Offset<flatbuffers::String>> *meshes = nullptr) {
  auto name__ = name ? _fbb.CreateString(name) : 0;
  auto empty_nodes__ = empty_nodes ? _fbb.CreateVector<flatbuffers::Offset<Engine::Graphics::PSPSceneNode>>(*empty_nodes) : 0;
  auto mesh_nodes__ = mesh_nodes ? _fbb.CreateVector<flatbuffers::Offset<Engine::Graphics::PSPMeshNode>>(*mesh_nodes) : 0;
  auto cameras__ = cameras ? _fbb.CreateVector<flatbuffers::Offset<Engine::Graphics::PSPCameraNode>>(*cameras) : 0;
  auto meshes__ = meshes ? _fbb.CreateVector<flatbuffers::Offset<flatbuffers::String>>(*meshes) : 0;
  return Engine::Graphics::CreatePSPScene(
      _fbb,
      name__,
      empty_nodes__,
      mesh_nodes__,
      cameras__,
      meshes__);
}

inline const Engine::Graphics::PSPScene *GetPSPScene(const void *buf) {
  return flatbuffers::GetRoot<Engine::Graphics::PSPScene>(buf);
}

inline const Engine::Graphics::PSPScene *GetSizePrefixedPSPScene(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<Engine::Graphics::PSPScene>(buf);
}

inline bool VerifyPSPSceneBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<Engine::Graphics::PSPScene>(nullptr);
}

inline bool VerifySizePrefixedPSPSceneBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<Engine::Graphics::PSPScene>(nullptr);
}

inline void FinishPSPSceneBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Engine::Graphics::PSPScene> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedPSPSceneBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Engine::Graphics::PSPScene> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Graphics
}  // namespace Engine

#endif  // FLATBUFFERS_GENERATED_PSPSCENE_ENGINE_GRAPHICS_H_
