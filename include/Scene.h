/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#ifndef __SCENE_H__
#define __SCENE_H__

#include <string>
#include <vector>

#include "PSPScene_generated.h"
#include "PSPMesh_generated.h"

struct BoundingBox {
	alignas(16) struct {
		float x, y, z;
	} verts[8];
};

class Scene {
	std::string source, source_root;
	std::vector<uint8_t*> mesh_map;
	uint8_t *scene_data;

	std::vector<BoundingBox> bboxes;
public:
	Scene(std::string filename);
	~Scene();

	void* (*alloc)(size_t);
	void (*release)(void*);

	void unloadAll();
	void unloadMeshes();
	void unloadScene();

	bool loadAll(bool calc_bb);
	bool loadMeshes(bool calc_bb);
	bool loadScene();

	const Engine::Graphics::PSPMesh *getMesh(int idx);
	const Engine::Graphics::PSPMeshNode *getMeshNode(int idx);
	int findMeshNode(std::string name);

	const Engine::Graphics::PSPScene *getScene();

	const Engine::Graphics::PSPCameraNode *getCamera(int idx);
	int findCamera(std::string name);

	const Engine::Graphics::PSPSceneNode *getEmpty(int idx);
	int findEmpty(std::string name);

	void activateCamera(int idx, float fov_override = -1.0f, float aspx_override = -1.0f);
	void drawAllMeshes();
};

#endif //__SCENE_H__