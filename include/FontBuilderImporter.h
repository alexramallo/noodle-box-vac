/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#ifndef __FONT_BUILDER_IMPORTER__H__
#define __FONT_BUILDER_IMPORTER__H__
#include <string>
/**
	Tools for working with bitmap fonts exported from Andrey AndryBlack Kunitsyn's FontBuilder tool
	tool url: https://github.com/andryblack/fontbuilder
*/


/**
	Uncomment the define below to enable allocating space for glyphs
	dynamically as needed. This is slower since glyphs need to be looked
	up in the storage buffer for rendering, but potentially uses less memory
	and can support more than 256 glyph types (although more can easily be
	supported by increasing the storage of the `glyphs` array in the
	`FontData` struct)
*/
//#define USE_DYNAMIC_GLYPH_STORAGE

namespace FontBuilder {

	typedef uint16_t 	gd_place_t;
	typedef uint8_t 	gd_char_t;
	typedef int16_t		gd_offset_t;
	typedef uint16_t 	gd_adv_t;

	struct GlyphData {
		gd_char_t ID;
		gd_place_t PlaceX, PlaceY;
		gd_place_t PlaceW, PlaceH;
		gd_offset_t OffsetX, OffsetY;
		gd_adv_t AdvanceX;
	};

	struct FontData {
		std::string family, filename;
		int size, lineHeight, ascender, descender;
		int numGlyphs;
		#if USE_DYNAMIC_GLYPH_STORAGE
			GlyphData *glyphs;
		#else
			GlyphData glyphs[256];
		#endif
	};

	/**
		Imports a FontData structure from the given source string `src`, and automatically
		allocates space to store the FontData structure and all the glyphs
	*/
	FontData *importFontDataEmbedded(const unsigned char *buffer, const int buffer_length);
	
	/**
		Frees the FontData loaded with `importFontData(src)`
	*/
	void freeFontData(FontData *);


};
#endif