/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#ifndef __PSP_SYS_CALLBACKS__H__
#define __PSP_SYS_CALLBACKS__H__

#include <pspkernel.h>

extern "C" {
	int exit_callback(int arg1, int arg2, void *common);
	int CallbackThread(SceSize args, void *argp);
	int SetupCallbacks(void);
}

#endif //__PSP_SYS_CALLBACKS__H__