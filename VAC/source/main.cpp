/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
/**
	Handles GLTF transformations using tinygltf
*/
#include "spdlog/spdlog.h"
#include "options.h"
#include "ops/Operation.h"
#include "ops/ExtractMesh.h"
#include "ops/ExtractPSPScene.h"
#include <structopt/app.hpp>

#include "util/VertexTransformer.h"

int main(int argc, char *argv[]){
	try { 
		auto options = structopt::app("vac").parse<Options>(argc, argv);
		spdlog::set_level(options.LogLevel.value());

		Operation *op = nullptr;

		if(options.extract_mesh.has_value()){
			spdlog::info("Running extract_mesh");
			op = new ExtractMesh();
		}

		if(options.extract_scene.has_value()){
			spdlog::info("Running extract_scene");
			op = new ExtractPSPScene();
		}

		if(op == nullptr){
			spdlog::error("ERROR: Missing sub-command");
			std::string help = "--help";
			char *arg[2] = {argv[0], (char*)help.c_str()};
			main(2, &arg[0]);
			return 1;
		}

		int ret = op->run(options);
		if(ret != 0){
			spdlog::critical("Error: Unspecified failure in operation");
			return 1;
		}else{
			delete op;
			return 0;
		}
	} catch (structopt::exception& e) {
		spdlog::error(e.what());
		spdlog::error(e.help());
		return 1;
	}
}
