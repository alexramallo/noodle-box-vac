/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#ifndef __VERTEX_TYPES__H__
#define __VERTEX_TYPES__H__

#include <cstddef>
#include <inttypes.h>

enum PrimitiveType {
	POINTS			= 0,
	LINES			= 1,
	LINE_LOOP		= 2,
	LINE_STRIP		= 3,
	TRIANGLES		= 4,
	TRIANGLE_STRIP	= 5,
	TRIANGLE_FAN	= 6
};

enum VDataType {
	DT_UBYTE	= 5,
	DT_USHORT	= 7,
	DT_FLOAT	= 9,
	DT_UINT		= 4,
	DT_BYTE		= 1,
	DT_SHORT	= 6,
	DT_INT		= 2,
	DT_NONE		= 0,

	DT_COL_5650	= 3,
	DT_COL_5551	= 1,
	DT_COL_4444	= 2,
	DT_COL_8888	= 4,
	DT_COL_NONE	= 0,

	//vec3/vec4 of floats in range [0.0, 1.0]
	DT_COL_RGB_UNBYTE	= 5,
	DT_COL_RGBA_UNBYTE	= 6,
	DT_COL_RGB_UNSHORT	= 7,
	DT_COL_RGBA_UNSHORT	= 8,
	DT_COL_RGB_FLOAT	= 9,
	DT_COL_RGBA_FLOAT	= 10,
};

constexpr VDataType VDataTypeForColor(VDataType component_type, size_t component_count){
	return static_cast<VDataType>(
		static_cast<int>(component_type) + 
		(static_cast<int>(component_count) - 3)
	);
}

enum VertexAttrib {
	VA_POSITION = 0,
	VA_NORMAL 	= 4,
	VA_COLOR 	= 8,
	VA_TEXTURE 	= 12,
	VA_WEIGHTS 	= 16,
	VA_INDEX    = 20,
	VA_INVALID
};

constexpr const char *VertexAttribName(VertexAttrib attrib){
	const char *s = "POS\0NRM\0COL\0TEX\0WGT\0IDX";
	return s + (size_t)attrib;
};

enum VertexAttribFormat {
	INDEX_NONE 			= (DT_NONE << VA_INDEX),
	INDEX_UBYTE 		= (DT_UBYTE << VA_INDEX),
	INDEX_USHORT		= (DT_USHORT << VA_INDEX),
	INDEX_UINT 			= (DT_UINT << VA_INDEX),

	POSITION_BYTE 		= (DT_BYTE << VA_POSITION),
	POSITION_SHORT 		= (DT_SHORT << VA_POSITION),
	POSITION_INT 		= (DT_INT << VA_POSITION),
	POSITION_FLOAT 		= (DT_FLOAT << VA_POSITION),

	NORMAL_NONE 		= (DT_NONE << VA_NORMAL),
	NORMAL_BYTE 		= (DT_BYTE << VA_NORMAL),
	NORMAL_SHORT 		= (DT_SHORT << VA_NORMAL),
	NORMAL_INT  		= (DT_INT << VA_NORMAL),
	NORMAL_FLOAT 		= (DT_FLOAT << VA_NORMAL),

	COLOR_NONE	 		= (DT_COL_NONE << VA_COLOR),
	COLOR_5650 			= (DT_COL_5650 << VA_COLOR),
	COLOR_5551	 		= (DT_COL_5551 << VA_COLOR),
	COLOR_4444 			= (DT_COL_4444 << VA_COLOR),
	COLOR_8888 			= (DT_COL_8888 << VA_COLOR),
	COLOR_FLOAT_RGBA	= (DT_COL_RGBA_FLOAT << VA_COLOR),
	COLOR_FLOAT_RGB		= (DT_COL_RGB_FLOAT << VA_COLOR),
	COLOR_UNBYTE_RGBA	= (DT_COL_RGBA_UNBYTE << VA_COLOR),
	COLOR_UNBYTE_RGB	= (DT_COL_RGB_UNBYTE << VA_COLOR),
	COLOR_UNSHORT_RGBA	= (DT_COL_RGBA_UNSHORT << VA_COLOR),
	COLOR_UNSHORT_RGB	= (DT_COL_RGB_UNSHORT << VA_COLOR),

	TEXTURE_NONE 		= (DT_NONE << VA_TEXTURE),
	TEXTURE_BYTE 		= (DT_BYTE << VA_TEXTURE),
	TEXTURE_SHORT 		= (DT_SHORT << VA_TEXTURE),
	TEXTURE_INT 		= (DT_INT << VA_TEXTURE),
	TEXTURE_FLOAT 		= (DT_FLOAT << VA_TEXTURE),

	WEIGHTS_NONE 		= (DT_NONE << VA_WEIGHTS),
	WEIGHTS_BYTE 		= (DT_BYTE << VA_WEIGHTS),
	WEIGHTS_SHORT 		= (DT_SHORT << VA_WEIGHTS),
	WEIGHTS_INT 		= (DT_INT << VA_WEIGHTS),
	WEIGHTS_FLOAT 		= (DT_FLOAT << VA_WEIGHTS),
};

enum CommonVertexFormats {
	VT_MESH 						= POSITION_FLOAT,
	VT_COLORED_MESH					= POSITION_FLOAT | COLOR_5551,
	VT_LIT_MESH 					= POSITION_FLOAT | NORMAL_FLOAT,
	VT_TEXTURED_MESH 				= POSITION_FLOAT | TEXTURE_FLOAT,
	VT_TEXTURED_LIT_MESH 			= POSITION_FLOAT | NORMAL_FLOAT | TEXTURE_FLOAT,
	VT_SKINNED_TEXTURED_LIT_MESH 	= POSITION_FLOAT | NORMAL_FLOAT | TEXTURE_FLOAT | WEIGHTS_FLOAT,
};

constexpr VDataType get_attribute_type(unsigned int format, unsigned int attribute){
	return static_cast<VDataType>((format >> attribute) & 15);
}

constexpr size_t num_vertex_attribute_components(unsigned int format, unsigned int attribute) {
	switch(attribute){
		case VA_POSITION: return 3;
		case VA_NORMAL:   return 3;
		case VA_TEXTURE:  return 2;
		case VA_WEIGHTS:  return 8;

		case VA_COLOR:
			switch((format >> VA_COLOR) & 15){
				default:					return 1;

				case DT_COL_RGB_UNBYTE:		return 3;
				case DT_COL_RGB_UNSHORT:	return 3;
				case DT_COL_RGB_FLOAT:		return 3;

				case DT_COL_RGBA_UNBYTE:	return 4;
				case DT_COL_RGBA_UNSHORT:	return 4;
				case DT_COL_RGBA_FLOAT:		return 4;
			}

		default: return 0;
	}
}

constexpr size_t sizeof_vertex_attribute_component(unsigned int format, unsigned int attribute) {
	size_t sz = 0;
	if(attribute == VA_COLOR){
		switch((format >> VA_COLOR) & 15){
			case DT_COL_NONE:
				break;
			case DT_COL_5650:
			case DT_COL_5551:
			case DT_COL_4444:
				sz = 2;
				break;
			case DT_COL_8888:
				sz = 4;
				break;
			case DT_COL_RGBA_FLOAT:
				sz = 4;
				break;
			case DT_COL_RGBA_UNBYTE:
				sz = 1;
				break;
			case DT_COL_RGBA_UNSHORT:
				sz = 2;
				break;
			case DT_COL_RGB_FLOAT:
				sz = 4;
				break;
			case DT_COL_RGB_UNBYTE:
				sz = 1;
				break;
			case DT_COL_RGB_UNSHORT:
				sz = 2;
				break;
		}
	}else{
		switch((format >> attribute) & 15){
			default: break;
			case DT_UBYTE:
			case DT_BYTE:
				sz = 1;
				break;
			case DT_USHORT:
			case DT_SHORT:
				sz = 2;
				break;
			case DT_UINT:
			case DT_INT:
				sz = 4;
				break;
			case DT_FLOAT:
				sz = 4;
				break;
		}
	}
	return sz;
}

constexpr size_t sizeof_vertex_attribute(unsigned int format, unsigned int attribute) {
	return sizeof_vertex_attribute_component(format, attribute) * num_vertex_attribute_components(format, attribute);
}

constexpr size_t sizeof_vertex(unsigned int format) {
	size_t ret = 0;
	ret += sizeof_vertex_attribute(format, VA_POSITION);
	ret += sizeof_vertex_attribute(format, VA_NORMAL);
	ret += sizeof_vertex_attribute(format, VA_COLOR);
	ret += sizeof_vertex_attribute(format, VA_TEXTURE);
	ret += sizeof_vertex_attribute(format, VA_WEIGHTS);
	return ret;
}

template<typename T>
constexpr T calc_alignment(T value, T alignment){
	T m = value % alignment;
	if(m){
		value += alignment - m;
	}
	return value;
}

/**
	Returns the largest component of all attributes in a vertex
*/
constexpr size_t get_largest_component_of_vertex_attributes(unsigned int format){
	size_t largest = 0;
	largest = std::max(largest, sizeof_vertex_attribute_component(format, VA_POSITION));
	largest = std::max(largest, sizeof_vertex_attribute_component(format, VA_NORMAL));
	largest = std::max(largest, sizeof_vertex_attribute_component(format, VA_COLOR));
	largest = std::max(largest, sizeof_vertex_attribute_component(format, VA_TEXTURE));
	largest = std::max(largest, sizeof_vertex_attribute_component(format, VA_WEIGHTS));
	return largest;
}

/**
	Works like sizeof_vertex, but returns a size with padding
	aligned to the largest attribute component
*/
constexpr size_t sizeof_vertex_aligned(unsigned int format) {
	size_t ret = sizeof_vertex(format);
	size_t alignment = get_largest_component_of_vertex_attributes(format);
	ret = calc_alignment(ret, alignment);
	return ret;
}

constexpr size_t sizeof_index_type(unsigned int format) {
	return sizeof_vertex_attribute_component(format, VA_INDEX);
}

static void test_bitfields(){
	static_assert(sizeof_index_type(VT_MESH | INDEX_UBYTE) == 1);
	static_assert(sizeof_index_type(VT_MESH | INDEX_USHORT) == 2);
	static_assert(sizeof_index_type(VT_MESH | INDEX_UINT) == 4);

	static_assert(VDataTypeForColor(DT_UBYTE, 3) == DT_COL_RGB_UNBYTE);
	static_assert(VDataTypeForColor(DT_UBYTE, 4) == DT_COL_RGBA_UNBYTE);
	static_assert(VDataTypeForColor(DT_USHORT, 3) == DT_COL_RGB_UNSHORT);
	static_assert(VDataTypeForColor(DT_USHORT, 4) == DT_COL_RGBA_UNSHORT);
	static_assert(VDataTypeForColor(DT_FLOAT, 3) == DT_COL_RGB_FLOAT);
	static_assert(VDataTypeForColor(DT_FLOAT, 4) == DT_COL_RGBA_FLOAT);

	static_assert(num_vertex_attribute_components(VT_MESH, VA_POSITION) == 3);
	static_assert(num_vertex_attribute_components(VT_MESH, VA_NORMAL) == 3);
	static_assert(num_vertex_attribute_components(VT_MESH, VA_COLOR) == 1);
	static_assert(num_vertex_attribute_components(VT_MESH | COLOR_FLOAT_RGBA, VA_COLOR) == 4);
	static_assert(num_vertex_attribute_components(VT_MESH | COLOR_FLOAT_RGB, VA_COLOR) == 3);
	static_assert(num_vertex_attribute_components(VT_MESH | COLOR_UNBYTE_RGBA, VA_COLOR) == 4);
	static_assert(num_vertex_attribute_components(VT_MESH | COLOR_UNBYTE_RGB, VA_COLOR) == 3);
	static_assert(num_vertex_attribute_components(VT_MESH | COLOR_UNSHORT_RGBA, VA_COLOR) == 4);
	static_assert(num_vertex_attribute_components(VT_MESH | COLOR_UNSHORT_RGB, VA_COLOR) == 3);

	static_assert(num_vertex_attribute_components(VT_MESH, VA_TEXTURE) == 2);
	static_assert(num_vertex_attribute_components(VT_MESH, VA_WEIGHTS) == 8);

	static_assert(sizeof_vertex_attribute(POSITION_BYTE, VA_POSITION) == 3);
	static_assert(sizeof_vertex_attribute(POSITION_SHORT, VA_POSITION) == 6);
	static_assert(sizeof_vertex_attribute(POSITION_INT, VA_POSITION) == 12);
	static_assert(sizeof_vertex_attribute(POSITION_FLOAT, VA_POSITION) == 12);

	static_assert(sizeof_vertex_attribute(NORMAL_NONE, VA_NORMAL) == 0);
	static_assert(sizeof_vertex_attribute(NORMAL_BYTE, VA_NORMAL) == 3);
	static_assert(sizeof_vertex_attribute(NORMAL_SHORT, VA_NORMAL) == 6);
	static_assert(sizeof_vertex_attribute(NORMAL_INT, VA_NORMAL) == 12);
	static_assert(sizeof_vertex_attribute(NORMAL_FLOAT, VA_NORMAL) == 12);

	static_assert(sizeof_vertex_attribute(COLOR_NONE, VA_COLOR) == 0);
	static_assert(sizeof_vertex_attribute(COLOR_5650, VA_COLOR) == 2);
	static_assert(sizeof_vertex_attribute(COLOR_5551, VA_COLOR) == 2);
	static_assert(sizeof_vertex_attribute(COLOR_4444, VA_COLOR) == 2);
	static_assert(sizeof_vertex_attribute(COLOR_8888, VA_COLOR) == 4);
	static_assert(sizeof_vertex_attribute(COLOR_FLOAT_RGB, VA_COLOR) == 12);
	static_assert(sizeof_vertex_attribute(COLOR_FLOAT_RGBA, VA_COLOR) == 16);
	static_assert(sizeof_vertex_attribute(COLOR_UNSHORT_RGB, VA_COLOR) == 6);
	static_assert(sizeof_vertex_attribute(COLOR_UNSHORT_RGBA, VA_COLOR) == 8);
	static_assert(sizeof_vertex_attribute(COLOR_UNBYTE_RGB, VA_COLOR) == 3);
	static_assert(sizeof_vertex_attribute(COLOR_UNBYTE_RGBA, VA_COLOR) == 4);

	static_assert(sizeof_vertex_attribute(TEXTURE_NONE, VA_TEXTURE) == 0);
	static_assert(sizeof_vertex_attribute(TEXTURE_BYTE, VA_TEXTURE) == 2);
	static_assert(sizeof_vertex_attribute(TEXTURE_SHORT, VA_TEXTURE) == 4);
	static_assert(sizeof_vertex_attribute(TEXTURE_INT, VA_TEXTURE) == 8);
	static_assert(sizeof_vertex_attribute(TEXTURE_FLOAT, VA_TEXTURE) == 8);

	static_assert(sizeof_vertex_attribute(WEIGHTS_NONE, VA_WEIGHTS) == 0);
	static_assert(sizeof_vertex_attribute(WEIGHTS_BYTE, VA_WEIGHTS) == 8);
	static_assert(sizeof_vertex_attribute(WEIGHTS_SHORT, VA_WEIGHTS) == 16);
	static_assert(sizeof_vertex_attribute(WEIGHTS_INT, VA_WEIGHTS) == 32);
	static_assert(sizeof_vertex_attribute(WEIGHTS_FLOAT, VA_WEIGHTS) == 32);

	{
		//                           1 * 3 = 3       1 * 4 = 4
		const auto v = INDEX_UINT | POSITION_BYTE | COLOR_8888;
		static_assert(sizeof_vertex(v) == 7);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 3);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 0);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 4);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 0);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 0);
	}
	{
		//              1 * 3 =  3      2 * 3 = 6      2            4 * 2 = 8       4 * 8 = 32    =  51
		const auto v = POSITION_BYTE | NORMAL_SHORT | COLOR_4444 | TEXTURE_FLOAT | WEIGHTS_FLOAT;
		static_assert(sizeof_vertex(v) == 51);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 3);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 6);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 2);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 8);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 32);
	}
	{
		//              2 * 3 =  6      1 * 3 = 3      2            2 * 2 = 4       1 * 8 = 8    =  51
		const auto v = POSITION_SHORT | NORMAL_BYTE | COLOR_5650 | TEXTURE_SHORT | WEIGHTS_BYTE;
		static_assert(sizeof_vertex(v) == 23);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 6);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 3);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 2);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 4);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 8);
	}
	{
		//              4 * 3 =  12      4 * 3 = 12     4            4 * 2 = 8       4 * 8 = 32    = 68 
		const auto v = POSITION_FLOAT | NORMAL_FLOAT | COLOR_8888 | TEXTURE_FLOAT | WEIGHTS_FLOAT;
		static_assert(sizeof_vertex(v) == 68);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 4);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 8);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 32);
	}
	{
		//              4 * 3 =  12    4 * 3 = 12   4            4 * 2 = 8       4 * 8 = 32    = 68 
		const auto v = POSITION_INT | NORMAL_INT | COLOR_8888 | TEXTURE_INT | WEIGHTS_INT;
		static_assert(sizeof_vertex(v) == 68);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 4);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 8);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 32);
	}
	{
		//              4 * 3 =  12    4 * 3 = 12   4 * 3 = 12        4 * 2 = 8     4 * 8 = 32    = 76 
		const auto v = POSITION_INT | NORMAL_INT | COLOR_FLOAT_RGB | TEXTURE_INT | WEIGHTS_INT;
		static_assert(sizeof_vertex(v) == 76);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 8);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 32);
	}
	{
		//              4 * 3 =  12    4 * 3 = 12   4 * 4 = 16         4 * 2 = 8     4 * 8 = 32    = 76 
		const auto v = POSITION_INT | NORMAL_INT | COLOR_FLOAT_RGBA | TEXTURE_INT | WEIGHTS_INT;
		static_assert(sizeof_vertex(v) == 80);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 16);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 8);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 32);
	}
	{
		const auto v = VT_LIT_MESH | INDEX_UBYTE;
		static_assert(sizeof_vertex(v) == 24);
		static_assert(sizeof_vertex_attribute(v, VA_POSITION) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_NORMAL) == 12);
		static_assert(sizeof_vertex_attribute(v, VA_COLOR) == 0);
		static_assert(sizeof_vertex_attribute(v, VA_TEXTURE) == 0);
		static_assert(sizeof_vertex_attribute(v, VA_WEIGHTS) == 0);
	}
}

#endif //__VERTEX_TYPES__H__