/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#include "VertexTransformer.h"
#include "util/VertexTypes.h"
#include <spdlog/spdlog.h>
#include <cassert>
#include <sstream>
#include <iomanip>
#include <limits>
#include <bitset>

void print_vertex_fmt(unsigned int fmt){
	spdlog::trace("vertex format: 0x{:x} (indices size: {} bytes)", fmt, sizeof_index_type(fmt));
	spdlog::trace("\tposition: {} x {} bytes", num_vertex_attribute_components(fmt, VA_POSITION), sizeof_vertex_attribute_component(fmt, VA_POSITION));
	spdlog::trace("\tnormal:   {} x {} bytes", num_vertex_attribute_components(fmt, VA_NORMAL), sizeof_vertex_attribute_component(fmt, VA_NORMAL));
	spdlog::trace("\tcolor:    {} x {} bytes", num_vertex_attribute_components(fmt, VA_COLOR), sizeof_vertex_attribute_component(fmt, VA_COLOR));
	spdlog::trace("\ttexture:  {} x {} bytes", num_vertex_attribute_components(fmt, VA_TEXTURE), sizeof_vertex_attribute_component(fmt, VA_TEXTURE));
	spdlog::trace("\tweights:  {} x {} bytes", num_vertex_attribute_components(fmt, VA_WEIGHTS), sizeof_vertex_attribute_component(fmt, VA_WEIGHTS));
	spdlog::trace("\tTOTAL: {} bytes", sizeof_vertex(fmt));
}

static std::string print_vdata(VertexAttrib attrib, unsigned int format, const uint8_t *p){
	VDataType type = static_cast<VDataType>((format >> attrib) & 15);
	std::stringstream ss;
	if(attrib == VA_COLOR){
		ss << "C";
		switch(type){
			case DT_COL_5650:
				ss << "5650("
					<< ""  << (int)(*((uint16_t*)p) & 0x1F)
					<< " " << (int)((*((uint16_t*)p)>>5) & 0x3F)
					<< " " << (int)((*((uint16_t*)p)>>11) & 0x1F)
					<< ")";
				break;
			case DT_COL_5551:
				ss << "5551("
					<< ""  << (int)(*((uint16_t*)p) & 0x1F)
					<< " " << (int)((*((uint16_t*)p)>>5) & 0x1F)
					<< " " << (int)((*((uint16_t*)p)>>10) & 0x1F)
					<< " " << (int)((*((uint16_t*)p)>>15) & 0x01)
					<< ")";
				break;
			case DT_COL_4444:
				ss << "4444("
					<< ""  << (int)(*((uint16_t*)p) & 0xF)
					<< " " << (int)((*((uint16_t*)p)>>4) & 0xF)
					<< " " << (int)((*((uint16_t*)p)>>8) & 0xF)
					<< " " << (int)((*((uint16_t*)p)>>12) & 0xF)
					<< ")";
				break;
			case DT_COL_8888:
				ss << "8888("
					<< ""  << (int)(*((uint32_t*)p) & 0xFF)
					<< " " << (int)((*((uint32_t*)p)>>8) & 0xFF)
					<< " " << (int)((*((uint32_t*)p)>>16) & 0xFF)
					<< " " << (int)((*((uint32_t*)p)>>24) & 0xFF)
					<< ")";
				break;
			case DT_COL_RGBA_UNBYTE:
				 ss << "RGBA_U8("
				 	<< ""  << (int)(((uint8_t*)p)[0])
				 	<< " " << (int)(((uint8_t*)p)[1])
					<< " " << (int)(((uint8_t*)p)[2])
					<< " " << (int)(((uint8_t*)p)[3])
					<< ")";
				break;
			case DT_COL_RGB_UNBYTE:
				 ss << "RGBA_U8("
				 	<< ""  << (int)(((uint8_t*)p)[0])
				 	<< " " << (int)(((uint8_t*)p)[1])
					<< " " << (int)(((uint8_t*)p)[2])
					<< ")";
				break;
			case DT_COL_RGBA_UNSHORT:
				 ss << "RGBA_U16("
				 	<< ""  << (int)(((uint16_t*)p)[0])
				 	<< " " << (int)(((uint16_t*)p)[1])
					<< " " << (int)(((uint16_t*)p)[2])
					<< " " << (int)(((uint16_t*)p)[3])
					<< ")";
				break;
			case DT_COL_RGB_UNSHORT:
				 ss << "RGBA_U16("
				 	<< ""  << (int)(((uint16_t*)p)[0])
				 	<< " " << (int)(((uint16_t*)p)[1])
					<< " " << (int)(((uint16_t*)p)[2])
					<< ")";
				break;
			case DT_COL_RGBA_FLOAT:
				ss << "RGBA_F("
					<< ""  << ((float*)p)[0]
					<< " " << ((float*)p)[1]
					<< " " << ((float*)p)[2]
					<< " " << ((float*)p)[3]
					<< ")";
				break;
			case DT_COL_RGB_FLOAT:
				ss << "RGB("
					<< ""  << ((float*)p)[0]
					<< " " << ((float*)p)[1]
					<< " " << ((float*)p)[2]
					<< ")";
				break;
			default:
				ss << "?(0x" << std::hex << (int)*p << ")";
				break;
		}
	}else{
		int num_comp;
		if(attrib == VA_INDEX){
			num_comp = 1;
		}else{
			num_comp = num_vertex_attribute_components(format, attrib);
		}

		for(int c = 0; c < num_comp; c++){
			switch(type){
				case DT_UBYTE:
					ss << std::to_string(*reinterpret_cast<const uint8_t*>(p));
					p += 1;
					break;
				case DT_USHORT:
					ss << std::to_string(*reinterpret_cast<const uint16_t*>(p));
					p += 2;
					break;
				case DT_UINT:
					ss << std::to_string(*reinterpret_cast<const uint32_t*>(p));
					p += 4;
					break;
				case DT_BYTE:
					ss << std::to_string(*reinterpret_cast<const int8_t*>(p));
					p += 1;
					break;
				case DT_SHORT:
					ss << std::to_string(*reinterpret_cast<const int16_t*>(p));
					p += 2;
					break;
				case DT_INT:
					ss << std::to_string(*reinterpret_cast<const int32_t*>(p));
					p += 4;
					break;
				case DT_FLOAT:
					ss << std::to_string((float)*reinterpret_cast<const float*>(p));
					p += 4;
					break;
				default:
					ss << "(" << (int)type << ")? 0x" << std::hex << (int)p[0];
					p += 1;
					break;
			}
			ss << ", ";
		}
	}
	return ss.str();
}

VertexTransformer::VertexTransformer():
	VertexTransformer(nullptr)
{
	//--
}

VertexTransformer::VertexTransformer(tinygltf::Mesh *m):
	VertexTransformer(m, std::numeric_limits<unsigned int>::max())
{
	//--
}
VertexTransformer::VertexTransformer(tinygltf::Mesh *m, unsigned int fmt):
	mesh(m),
	output_format(fmt)
{
	//--
}

int VertexTransformer::CreateIndexedVertexBufferFromPrim(
	AttribReadBufferSet &input_set
){
	spdlog::trace("CreateIndexedVertexBufferFromPrim()");
	spdlog::trace("output format:");
	print_vertex_fmt(output_format);

	bool is_input_indexed = input_set.indices != nullptr;
	spdlog::trace("is_input_indexed: {}", is_input_indexed);
	bool is_output_indexed = sizeof_index_type(output_format) != 0;
	spdlog::trace("is_output_indexed: {}", is_output_indexed);
	bool needs_unindexing = is_input_indexed && !is_output_indexed;
	spdlog::trace("needs_unindexing: {}", needs_unindexing);

	if(!is_input_indexed && is_output_indexed){
		spdlog::critical("Cannot apply indexing to an unindexed input buffer");
		return 1;
	}

	return do_transformation(input_set);
}

struct conversion_metrics {
	size_t num_indices;
	size_t num_verts;

	size_t output_vertex_size;
	size_t output_index_size;
	size_t output_vertex_buffer_size;
	size_t output_index_buffer_size;
	size_t output_vertex_size_aligned;
	size_t output_vertex_buffer_size_aligned;
	size_t output_vertex_buffer_size_wasted; //bytes wasted due to padding

	size_t input_vertex_size;
	size_t input_index_size;
	size_t input_vertex_buffer_size;
	size_t input_index_buffer_size;

	void calculate(VertexTransformer::AttribReadBufferSet &input_set, unsigned int output_format){
		num_verts = input_set.position->count;
		assert(((output_format >> VA_INDEX) & 15) != INDEX_NONE && "This function supports indexed output only");
		num_indices = input_set.indices->count;

		spdlog::trace("Input buffers");
		input_index_size = sizeof_index_type(input_set.indices->format);
		//calculate size per-vertex
		input_vertex_size = 0;
		for(int i = 0; i < 6; i++){
			if(input_set.buffers[i] != nullptr){
				input_vertex_size += sizeof_vertex(input_set.buffers[i]->format);
				spdlog::trace("\tbuffer[{}] count: {}", VertexAttribName((VertexAttrib)(i*4)), input_set.buffers[i]->count);
			}else{
				spdlog::trace("\tbuffer[{}] count: 0", VertexAttribName((VertexAttrib)(i*4)), 0);
			}
		}
		input_vertex_buffer_size = input_vertex_size * num_verts;
		input_index_buffer_size = input_index_size * num_indices;

		output_vertex_size = sizeof_vertex(output_format);
		output_index_size = sizeof_index_type(output_format);
		output_vertex_buffer_size = output_vertex_size * num_verts;
		output_index_buffer_size = output_index_size * num_indices;

		output_vertex_size_aligned = sizeof_vertex_aligned(output_format);
		output_vertex_buffer_size_aligned = output_vertex_size_aligned * num_verts;

		output_vertex_buffer_size_wasted = output_vertex_buffer_size_aligned - output_vertex_buffer_size;

		spdlog::trace("conversion metrics:");
		spdlog::trace("\tnum_indices: {}", num_indices);
		spdlog::trace("\tnum_verts: {}", num_verts);
		spdlog::trace("\tinput_vertex_size: {}", input_vertex_size);
		spdlog::trace("\tinput_index_size: {}", input_index_size);
		spdlog::trace("\tinput_vertex_buffer_size: {}", input_vertex_buffer_size);
		spdlog::trace("\tinput_index_buffer_size: {}", input_index_buffer_size);
		spdlog::trace("\toutput_vertex_size: {}", output_vertex_size);
		spdlog::trace("\toutput_vertex_size_aligned: {}", output_vertex_size_aligned);
		spdlog::trace("\toutput_index_size: {}", output_index_size);
		spdlog::trace("\toutput_index_buffer_size: {}", output_index_buffer_size);
		spdlog::trace("\toutput_vertex_buffer_size: {}", output_vertex_buffer_size);
		spdlog::trace("\toutput_vertex_buffer_size_aligned: {}", output_vertex_buffer_size_aligned);
		if(output_vertex_buffer_size_wasted > 0){
			spdlog::warn("You are wasting space due to alignment requirements:");
			spdlog::warn("\toutput_vertex_buffer_size (packed): {}", output_vertex_buffer_size);
			spdlog::warn("\toutput_vertex_buffer_size (aligned): {}", output_vertex_buffer_size_aligned);
			spdlog::warn("\tdifference (i.e. wasted/padding bytes): {}", output_vertex_buffer_size_wasted);
			spdlog::warn("\tConsider choosing a more efficient output format");
		}else{
			spdlog::trace("\toutput_vertex_buffer_size_wasted: {}", output_vertex_buffer_size_wasted);
		}
	}
};

/**
	Type conversions for colors

	This assumes all integer types are unsigned and normalized
	See: https://www.khronos.org/opengl/wiki/Normalized_Integer

	float = int / (2^b - 1)
*/
static size_t convert_color(const uint8_t *from, uint8_t *to, VDataType type_from, VDataType type_to){
	spdlog::trace("convert_color (from: {}, to: {})", type_from, type_to);
	float col_from[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	switch(type_from){
		case DT_COL_5650:
			col_from[0] = static_cast<float>(((*((uint16_t*)from)) & 0x1F)) / 31.0f;
			col_from[1] = static_cast<float>(((*((uint16_t*)from)>>5) & 0x3F)) / 63.0f;
			col_from[2] = static_cast<float>(((*((uint16_t*)from)>>11) & 0x1F)) / 31.0f;
			col_from[3] = 1.0f;
			break;
		case DT_COL_5551:
			col_from[0] = static_cast<float>(((*((uint16_t*)from)) & 0x1F)) / 31.0f;
			col_from[1] = static_cast<float>(((*((uint16_t*)from)>>5) & 0x1F)) / 31.0f;
			col_from[2] = static_cast<float>(((*((uint16_t*)from)>>10) & 0x1F)) / 31.0f;
			col_from[3] = ((*((uint16_t*)from)>>15) & 0x01) ? 1.0f : 0.0f;
			break;
		case DT_COL_4444:
			col_from[0] = static_cast<float>(((*((uint16_t*)from)) & 0xF)) / 15.0f;
			col_from[1] = static_cast<float>(((*((uint16_t*)from)>>4) & 0xF)) / 15.0f;
			col_from[2] = static_cast<float>(((*((uint16_t*)from)>>8) & 0xF)) / 15.0f;
			col_from[3] = static_cast<float>(((*((uint16_t*)from)>>12) & 0xF)) / 15.0f;
			break;
		case DT_COL_8888:
			col_from[0] = static_cast<float>(((*((uint32_t*)from)) & 0xFF)) / 255.0f;
			col_from[1] = static_cast<float>(((*((uint32_t*)from)>>8) & 0xFF)) / 255.0f;
			col_from[2] = static_cast<float>(((*((uint32_t*)from)>>16) & 0xFF)) / 255.0f;
			col_from[3] = static_cast<float>(((*((uint32_t*)from)>>24) & 0xFF)) / 255.0f;
			break;
		case DT_COL_RGBA_UNBYTE:
			col_from[0] = ((uint8_t*)from)[0] / 255.0f;
			col_from[1] = ((uint8_t*)from)[1] / 255.0f;
			col_from[2] = ((uint8_t*)from)[2] / 255.0f;
			col_from[3] = ((uint8_t*)from)[3] / 255.0f;
			break;
		case DT_COL_RGB_UNBYTE:
			col_from[0] = ((uint8_t*)from)[0] / 255.0f;
			col_from[1] = ((uint8_t*)from)[1] / 255.0f;
			col_from[2] = ((uint8_t*)from)[2] / 255.0f;
			col_from[3] = 1.0f;
			break;
		case DT_COL_RGBA_UNSHORT:
			col_from[0] = ((uint16_t*)from)[0] / 65535.0f;
			col_from[1] = ((uint16_t*)from)[1] / 65535.0f;
			col_from[2] = ((uint16_t*)from)[2] / 65535.0f;
			col_from[3] = ((uint16_t*)from)[3] / 65535.0f;
			break;
		case DT_COL_RGB_UNSHORT:
			col_from[0] = ((uint16_t*)from)[0] / 65535.0f;
			col_from[1] = ((uint16_t*)from)[1] / 65535.0f;
			col_from[2] = ((uint16_t*)from)[2] / 65535.0f;
			col_from[3] = 1.0f;
			break;
		case DT_COL_RGBA_FLOAT:
			col_from[0] = ((float*)from)[0];
			col_from[1] = ((float*)from)[1];
			col_from[2] = ((float*)from)[2];
			col_from[3] = ((float*)from)[3];
			break;
		case DT_COL_RGB_FLOAT:
			col_from[0] = ((float*)from)[0];
			col_from[1] = ((float*)from)[1];
			col_from[2] = ((float*)from)[2];
			break;
		default:
			assert(0 && "Unrecognized color format");
			break;
	}

	const float E = 0.00001f;
	for(int i = 0; i < 4; i++){
		spdlog::trace("\tcol_from[{}]: {}", i, col_from[i]);
		assert(col_from[i] >= 0.0f - E && col_from[i] <= 1.0f + E);
		if(col_from[i] < 0.0f) col_from[i] = 0.0f;
		if(col_from[i] > 1.0f) col_from[i] = 1.0f;
	}

	switch(type_to){
		case DT_COL_5650:{
				uint16_t *out = (uint16_t *)to;
				*out |= static_cast<uint16_t>(roundf(col_from[0] * 31.0f));
				*out |= static_cast<uint16_t>(roundf(col_from[1] * 63.0f)) << 5;
				*out |= static_cast<uint16_t>(roundf(col_from[2] * 31.0f)) << 11;
				return 2;
			}
		case DT_COL_5551:{
				uint16_t *out = (uint16_t *)to;
				*out |= static_cast<uint16_t>(roundf(col_from[0] * 31.0f));
				*out |= static_cast<uint16_t>(roundf(col_from[1] * 31.0f)) << 5;
				*out |= static_cast<uint16_t>(roundf(col_from[2] * 31.0f)) << 10;
				if(col_from[3] > 0.0f){
					*out |= 1;
				}
				return 2;
			}
		case DT_COL_4444:{
				uint16_t *out = (uint16_t *)to;
				*out |= static_cast<uint16_t>(roundf(col_from[0] * 15.0f));
				*out |= static_cast<uint16_t>(roundf(col_from[1] * 15.0f)) << 4;
				*out |= static_cast<uint16_t>(roundf(col_from[2] * 15.0f)) << 8;
				*out |= static_cast<uint16_t>(roundf(col_from[3] * 15.0f)) << 12;
				return 2;
			}
		case DT_COL_8888:{
				uint32_t *out = (uint32_t *)to;
				*out |= static_cast<uint32_t>(roundf(col_from[0] * 255.0f));
				*out |= static_cast<uint32_t>(roundf(col_from[1] * 255.0f)) << 8;
				*out |= static_cast<uint32_t>(roundf(col_from[2] * 255.0f)) << 16;
				*out |= static_cast<uint32_t>(roundf(col_from[3] * 255.0f)) << 24;
				return 4;
			}
		case DT_COL_RGBA_UNBYTE:{
				uint16_t *out = (uint16_t *)to;
				out[0] = col_from[0] * 255.0f;
				out[1] = col_from[1] * 255.0f;
				out[2] = col_from[2] * 255.0f;
				out[3] = col_from[3] * 255.0f;
				return 4;
			}
		case DT_COL_RGB_UNBYTE:{
				uint16_t *out = (uint16_t *)to;
				out[0] = col_from[0] * 255.0f;
				out[1] = col_from[1] * 255.0f;
				out[2] = col_from[2] * 255.0f;
				return 3;
			}
		case DT_COL_RGBA_UNSHORT:{
				uint16_t *out = (uint16_t *)to;
				out[0] = col_from[0] * 65535.0f;
				out[1] = col_from[1] * 65535.0f;
				out[2] = col_from[2] * 65535.0f;
				out[3] = col_from[3] * 65535.0f;
				return 8;
			}
		case DT_COL_RGB_UNSHORT:{
				uint16_t *out = (uint16_t *)to;
				out[0] = col_from[0] * 65535.0f;
				out[1] = col_from[1] * 65535.0f;
				out[2] = col_from[2] * 65535.0f;
				return 6;
			}
		case DT_COL_RGBA_FLOAT:{
				float *out = (float *)to;
				out[0] = col_from[0];
				out[1] = col_from[1];
				out[2] = col_from[2];
				out[3] = col_from[3];
				return 16;
			}
		case DT_COL_RGB_FLOAT:{
				float *out = (float *)to;
				out[0] = col_from[0];
				out[1] = col_from[1];
				out[2] = col_from[2];
				return 12;
			}
		default:
			assert(0 && "Unrecognized color format");
			break;
	}
	return 0;
}

/*
	Copies index/vertex data from input buffer to destination buffer
	Responsible for performing data conversions
	
	TODO: output endianness conversion? (GLTF is always LE)
*/
static size_t copy_vdata(VertexAttrib attrib, const uint8_t *from, uint8_t *to, VDataType fmt_from, VDataType fmt_to){
	spdlog::trace("copy_vdata");
	if(attrib == VA_COLOR){
		spdlog::trace("\tVA_COLOR!");
		size_t ret = convert_color(from, to, fmt_from, fmt_to);
		assert(ret == sizeof_vertex_attribute(fmt_to << VA_COLOR, VA_COLOR));
		return ret;
	}

	size_t sz_from = sizeof_vertex_attribute_component(fmt_from, 0);
	size_t sz_to = sizeof_vertex_attribute_component(fmt_to, 0);

	int num_components;
	if(attrib == VA_INDEX){
		num_components = 1;
	}else{
		num_components = num_vertex_attribute_components(fmt_to, attrib);
	}

	if(fmt_from == DT_FLOAT || fmt_to == DT_FLOAT){
		if(fmt_from == fmt_to){
			//both floats, it's all good
			size_t copy_amount = sz_to * num_components;
			spdlog::trace("\tfloat-to-float ({} bytes)", copy_amount);
			memcpy(to, from, copy_amount);
			return copy_amount;
		}else if(fmt_to == DT_FLOAT){
			//need to upgrade 'from' integer to output 'to' float
			size_t copy_amount = 0;
			for(int i = 0; i < num_components; i++){
				float *out = reinterpret_cast<float*>(to) + i;
				float flt = 0.0f;
				switch(fmt_from){
					case DT_UBYTE:  flt = (float) *(reinterpret_cast<const uint8_t*>(from)+i); break;
					case DT_BYTE:   flt = (float) *(reinterpret_cast<const int8_t*>(from)+i); break;
					case DT_USHORT: flt = (float) *(reinterpret_cast<const uint16_t*>(from)+i); break;
					case DT_SHORT:  flt = (float) *(reinterpret_cast<const int16_t*>(from)+i); break;
					case DT_UINT:   flt = (float) *(reinterpret_cast<const uint32_t*>(from)+i); break;
					case DT_INT:    flt = (float) *(reinterpret_cast<const int32_t*>(from)+i); break;
					default: assert(0 && "invalid conversion");
				}
				//TODO: scale, normalize, etc?
				*out = flt;
				copy_amount += sizeof(float);
			}
			spdlog::trace("\tint to float ({} bytes)", copy_amount);
			return copy_amount;
		}else{
			spdlog::trace("\tfloat to int");
			//need to downgrade 'from' float to output 'to' integer
			size_t copy_size = sz_to < sizeof(int32_t) ? sz_to : sizeof(int32_t);
			size_t copy_amount = 0;
			for(int i = 0; i < num_components; i++){
				int32_t from_i = static_cast<int32_t>(roundf(*(reinterpret_cast<const float*>(from)+i)));
				memcpy(to + (sz_to*i), &from_i, copy_size);
				copy_amount += copy_size;
			}
			return copy_amount;
		}
	}
	
	if(sz_from == sz_to){
		spdlog::trace("\tints, same size");
		memcpy(to, from, sz_to * num_components);
		return sz_to * num_components;
	}else if(sz_from > sz_to){
		spdlog::trace("\tints, from > to");
		//Shrink ('from' is larger than 'to'), need to truncate
		for(int i = 0; i < num_components; i++){
			memcpy(to + (sz_to * i), from + (sz_from * i), sz_to);
		}
		return sz_to * num_components;
	}else{
		spdlog::trace("\tints, from < to");
		//Grow ('from' is smaller than 'to')
		for(int i = 0; i < num_components; i++){
			memcpy(to + (sz_to * i), from + (sz_from * i), sz_from);
		}
		return sz_to * num_components;
	}
	return 0;
}

static size_t do_copy_buffer(VertexAttrib attrib, int count, size_t input_stride, size_t output_stride, const uint8_t *input_buf, uint8_t *output_buf, VDataType type_in, VDataType type_out){
	size_t copy_ct = 0;
	spdlog::trace("do_copy_buffer({}) {}", count, VertexAttribName(attrib));
	spdlog::trace("\tstride_in: {}, stride_out: {}", input_stride, output_stride);
	for(int i = 0; i < count; i++){
		const uint8_t *pFrom = &input_buf[i * input_stride];
		uint8_t *pTo = &output_buf[i * output_stride];
		copy_ct += copy_vdata(attrib, pFrom, pTo, type_in, type_out);
		spdlog::trace("\tcopy[{}] {} => {}", i,
			print_vdata(attrib, type_in << attrib, pFrom),
			print_vdata(attrib, type_out << attrib, pTo)
		);
	}
	return copy_ct;
}

template<VertexAttrib ATTRIB>
int do_copy_attribute_buffer(size_t element_count, unsigned int output_format, VertexTransformer::AttributeReadBufferInfo *readbuf, uint8_t *output_buffer){
	VDataType fmt_out = static_cast<VDataType>((output_format >> ATTRIB) & 15);
	
	size_t sz_comp_in;
	size_t num_components_in;
	size_t stride_in;

	size_t sz_comp_out;
	size_t num_components_out;
	size_t stride_out;

	if(ATTRIB == VA_INDEX){
		sz_comp_out = sizeof_index_type(output_format);
		num_components_out = 1;
		stride_out = sz_comp_out;
	}else{
		sz_comp_out = sizeof_vertex_attribute_component(output_format, ATTRIB);
		num_components_out = num_vertex_attribute_components(output_format, ATTRIB);
		stride_out = sizeof_vertex_attribute(output_format, ATTRIB);
	}

	//Does output require this attribute data?
	if(sz_comp_out > 0){
		//Does input have this attribute data?
		if(readbuf != nullptr){

			if(ATTRIB == VA_INDEX){
				sz_comp_in = sizeof_index_type(readbuf->format);
				num_components_in = 1;
			}else{
				sz_comp_in = sizeof_vertex_attribute_component(readbuf->format, ATTRIB);
				num_components_in = num_vertex_attribute_components(readbuf->format, ATTRIB);
			}
			stride_in = num_components_in * sz_comp_in;

			spdlog::trace("stride_in: {}, sz_comp_in: {}, num_components_in: {}",
				stride_in, sz_comp_in, num_components_in
			);

			VDataType fmt_in = static_cast<VDataType>((readbuf->format >> ATTRIB) & 15);

			size_t bytes_copied = do_copy_buffer(
				ATTRIB,
				element_count,
				stride_in, //input buffer stride
				stride_out, //output buffer stride
				&readbuf->buffer[0],
				output_buffer,
				fmt_in,
				fmt_out
			);
			return bytes_copied;
		}else{
			//Output wants attribute data that isn't in input, so pad output with zeroes
			//...but output is already initialized with zeros, so just return expected count
			return sizeof_vertex_attribute(output_format, ATTRIB) * element_count;
		}
	}
	return 0;
}

int VertexTransformer::do_transformation(AttribReadBufferSet &input_set){
	if(do_extraction(input_set)){
		spdlog::error("Failure during data extraction from input buffers");
		return 1;
	}

	conversion_metrics metrics;
	metrics.calculate(input_set, output_format);
	num_vertices = metrics.num_verts;
	num_indices = metrics.num_indices;
	vertex_stride = metrics.output_vertex_size_aligned;

	if(true){ //do alignment?
		size_t alignment = get_largest_component_of_vertex_attributes(output_format);

		#define _ALIGN_BUF(ATTRIB, buf)														\
			if(buf.size() > 0){														 		\
				size_t sz = sizeof_vertex_attribute_component(output_format, ATTRIB);		\
				size_t num_comps = num_vertex_attribute_components(output_format, ATTRIB);	\
				size_t pre_align_size = buf.size();											\
				do_align_buffer(buf, sz, num_comps * num_vertices, alignment);				\
				size_t post_align_size = buf.size();										\
				spdlog::info("Aligned " #ATTRIB " buffer. Size {} -> {} (d: {})", pre_align_size, post_align_size, post_align_size - pre_align_size);\
			}

		_ALIGN_BUF(VA_POSITION, exc_position);
		_ALIGN_BUF(VA_NORMAL, exc_normal);
		_ALIGN_BUF(VA_COLOR, exc_color);
		_ALIGN_BUF(VA_TEXTURE, exc_texture);
		_ALIGN_BUF(VA_WEIGHTS, exc_weights);
	}

	if(do_interleave_attributes(input_set)){
		spdlog::error("Failure during interleaving of attributes");
		return 2;
	}
	return 0;
}

int VertexTransformer::do_interleave_attributes(AttribReadBufferSet &input_set){
	spdlog::trace("VertexTransformer::do_interleave_attributes");
	conversion_metrics metrics;
	metrics.calculate(input_set, output_format);

	out_vertices_interleaved.resize(metrics.output_vertex_buffer_size_aligned);

	//TODO: allow packed output?
	//size_t vertex_stride = metrics.output_vertex_size;
	// out_vertices_interleaved.resize(metrics.output_vertex_buffer_size);

	uint8_t *vertex = &out_vertices_interleaved[0];

	size_t alignment = get_largest_component_of_vertex_attributes(output_format);

	size_t bytes_copied = 0;

	for(int i = 0; i < metrics.num_verts; i++){
		uint8_t *vertex = &out_vertices_interleaved[i * vertex_stride];
		size_t attribute_offset = 0;
		spdlog::trace("Vertex[{0}] +0x{1:x} ({1:d})", i, i * vertex_stride);

		#define HANDLE_VATTRIB(ATTRIB, READBUF)														\
			if(sizeof_vertex_attribute_component(output_format, ATTRIB) > 0){						\
				size_t num_comp = num_vertex_attribute_components(output_format, ATTRIB); 			\
				size_t comp_stride = sizeof_vertex_attribute_component(output_format, ATTRIB);		\
				size_t vert_size = sizeof_vertex_attribute(output_format, ATTRIB); 					\
				size_t aligned_size = calc_alignment(vert_size, alignment);							\
				memcpy(vertex + attribute_offset, &READBUF[i*aligned_size], aligned_size);			\
				bytes_copied += aligned_size;														\
				spdlog::trace("\t(+{}) " #ATTRIB "({} bytes) {}", (size_t)(vertex + attribute_offset)-((size_t)&out_vertices_interleaved[0]),aligned_size, print_vdata(ATTRIB, output_format, vertex + attribute_offset));\
				attribute_offset += aligned_size;													\
			}
		HANDLE_VATTRIB(VA_WEIGHTS, exc_weights)
		HANDLE_VATTRIB(VA_TEXTURE, exc_texture)
		HANDLE_VATTRIB(VA_COLOR, exc_color)
		HANDLE_VATTRIB(VA_NORMAL, exc_normal)
		HANDLE_VATTRIB(VA_POSITION, exc_position)
		#undef HANDLE_VATTRIB
	}

	spdlog::info("Copied a total of {} bytes of interleaved vertex data for {} vertices", bytes_copied, metrics.num_verts);

	return 0;
}

int VertexTransformer::do_extraction(AttribReadBufferSet &input_set){
	spdlog::trace("VertexTransformer::do_extraction");
	conversion_metrics metrics;
	metrics.calculate(input_set, output_format);

	size_t bytes_copied = 0;

	//Index buffer
	size_t num_index_out = num_vertex_attribute_components(output_format, VA_INDEX);
	size_t num_index_in = num_vertex_attribute_components(input_set.indices->format, VA_INDEX);

	if(num_index_in == 0 && num_index_out != 0){
		spdlog::error("Cannot convert un-indexed input to indexed output");
		return 1;
	}

	if(num_index_in != 0 && num_index_out == 0){
		spdlog::error("Indexed-to-NonIndexed conversion not implemented yet!");
		return 1;
	}

	if(num_index_in != num_index_out){
		spdlog::error("Input index count ({}) doesn't match output index count ({})", num_index_in, num_index_out);
		return 1;
	}

	size_t total_index_bytes = 0;
	size_t total_vertex_bytes = 0;

	//Index data
	exc_indices.resize(metrics.output_index_buffer_size);
	total_index_bytes = bytes_copied = do_copy_attribute_buffer<VA_INDEX>(
		metrics.num_indices,
		output_format,
		input_set.indices,
		&exc_indices[0]
	);
	spdlog::trace("Copied {} bytes of index data", bytes_copied);

	#define HANDLE_VATTRIB(ATTRIB, READBUF, OUTBUF)\
		OUTBUF.resize(sizeof_vertex_attribute(output_format, ATTRIB) * metrics.num_verts);				\
		spdlog::trace("size of outbuf(" #OUTBUF "): {}", OUTBUF.size());								\
		total_vertex_bytes += bytes_copied = do_copy_attribute_buffer<ATTRIB>(							\
			metrics.num_verts,																			\
			output_format,																				\
			READBUF,																					\
			&OUTBUF[0]																					\
		);																								\
		spdlog::trace("Copied {} bytes of " #ATTRIB " attribute data", bytes_copied);					\

	//TODO: implement a way to change order of these...

	HANDLE_VATTRIB(VA_POSITION, input_set.position, exc_position)
	HANDLE_VATTRIB(VA_NORMAL, input_set.normal, exc_normal)
	HANDLE_VATTRIB(VA_COLOR, input_set.color, exc_color)
	HANDLE_VATTRIB(VA_TEXTURE, input_set.texture, exc_texture)
	HANDLE_VATTRIB(VA_WEIGHTS, input_set.weights, exc_weights)
	#undef HANDLE_VATTRIB

	spdlog::info("Total index bytes: {}", total_index_bytes);
	spdlog::info("Total vertex bytes: {}", total_vertex_bytes);

	return 0;
}

int VertexTransformer::do_align_buffer(std::vector<uint8_t> &buf, size_t element_size, size_t element_count, size_t alignment){
	size_t aligned_size = calc_alignment(element_size, alignment);
	std::vector<uint8_t> outbuf(aligned_size * element_count, 0);

	spdlog::trace("do_align_buffer: element_size ({}), element_count({}), alignment({}), outbuf size({})", element_size, element_count, alignment, outbuf.size());

	for(int i = 0; i < element_count; i++){
		memcpy(
			&outbuf[i * aligned_size],
			&buf[i * element_size],
			element_size
		);
	}

	buf.swap(outbuf);
	return 0;
}
