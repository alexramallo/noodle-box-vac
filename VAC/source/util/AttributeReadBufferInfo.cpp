/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#include "VertexTransformer.h"
#include "util/VertexTypes.h"
#include <spdlog/spdlog.h>
#include <cassert>

static const char *attrib_to_key(VertexAttrib attrib){
	switch(attrib){
		case VA_POSITION: 	return "POSITION";
		case VA_NORMAL: 	return "NORMAL";
		case VA_COLOR: 		return "COLOR_0";
		case VA_TEXTURE: 	return "TEXCOORD_0";
		case VA_WEIGHTS: 	return "WEIGHTS_0";
		default:
			spdlog::critical("attrib_to_key: bad attribute type {}", attrib);
			return nullptr;
	}
}

static VertexAttrib key_to_attrib(std::string attrib_key){
	if(attrib_key == "POSITION") return VA_POSITION;
	if(attrib_key == "NORMAL") return VA_NORMAL;
	if(attrib_key == "COLOR_0") return VA_COLOR;
	if(attrib_key == "TEXCOORD_0") return VA_TEXTURE;
	if(attrib_key == "WEIGHTS_0") return VA_WEIGHTS;
	spdlog::critical("key_to_attrib: bad attribute key '{}'", attrib_key);
	return VA_INVALID;
}

VertexTransformer::AttributeReadBufferInfo VertexTransformer::AttributeReadBufferInfo::FromGLTFAccessor(
	tinygltf::Model &model,
	const tinygltf::Accessor &accessor,
	VertexAttrib attrib
){
	VertexTransformer::AttributeReadBufferInfo ret;
	ret.buffer = nullptr;
	ret.buffer_length = 0;
	ret.count = accessor.count;

	//Get buffer pointer
	const tinygltf::BufferView &bufferView = model.bufferViews[accessor.bufferView];
	const tinygltf::Buffer &buffer = model.buffers[bufferView.buffer];
	ret.buffer = &buffer.data[bufferView.byteOffset + accessor.byteOffset];

	//Get vertex format
	VDataType component_type;
	switch (accessor.componentType) {
		case TINYGLTF_COMPONENT_TYPE_BYTE: 				component_type = DT_BYTE; break;
		case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE: 	component_type = DT_UBYTE; break;
		case TINYGLTF_COMPONENT_TYPE_SHORT: 			component_type = DT_SHORT; break;
		case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT: 	component_type = DT_USHORT; break;
		case TINYGLTF_COMPONENT_TYPE_INT: 				component_type = DT_INT; break; //not in spec?
		case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT: 		component_type = DT_UINT; break;
		case TINYGLTF_COMPONENT_TYPE_FLOAT: 			component_type = DT_FLOAT; break;
	}

	size_t num_components = 0;
	switch(accessor.type){
		case TINYGLTF_TYPE_SCALAR: num_components = 1; break;
		case TINYGLTF_TYPE_VEC2:   num_components = 2; break;
		case TINYGLTF_TYPE_VEC3:   num_components = 3; break;
		case TINYGLTF_TYPE_VEC4:   num_components = 4; break;
		case TINYGLTF_TYPE_MAT2:   num_components = 4; break;
		case TINYGLTF_TYPE_MAT3:   num_components = 9; break;
		case TINYGLTF_TYPE_MAT4:   num_components = 16; break;
	}

	if(attrib == VA_INDEX){
		ret.format = static_cast<VertexAttribFormat>(component_type << attrib);
		ret.buffer_length = sizeof_index_type(ret.format);
	}else{

		if(attrib == VA_COLOR){
			switch(component_type){
				default:
					spdlog::error("Invalid component type for color attribute: {}", component_type);
					ret.buffer = nullptr;
					ret.buffer_length = 0;
					return ret;

				case DT_UBYTE:
				case DT_USHORT:
				case DT_FLOAT:
					component_type = VDataTypeForColor(component_type, num_components);
					break;
			}
		}

		ret.format = static_cast<VertexAttribFormat>(component_type << attrib);

		spdlog::trace("num_vertex_attribute_components(ret.format, attrib)({}) == num_components({})",
			num_vertex_attribute_components(ret.format, attrib),
			num_components
		);
		assert(num_vertex_attribute_components(ret.format, attrib) == num_components
			&& "Mismatched component count for attribute");
		ret.buffer_length = sizeof_vertex_attribute(ret.format, attrib) * accessor.count;
	}

	return ret;
}


VertexTransformer::AttributeReadBufferInfo VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(
	tinygltf::Model &model,
	tinygltf::Primitive &primitive,
	std::string attrib_key
){
	VertexAttrib attrib = key_to_attrib(attrib_key);
	if(attrib == VA_INVALID){
		spdlog::warn("Requested attribute '{}' not found in primitive");
		VertexTransformer::AttributeReadBufferInfo ret;
		ret.buffer = nullptr;
		return ret;
	}
	return FromGLTFPrimitive(model, primitive, attrib);
}

VertexTransformer::AttributeReadBufferInfo VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(
	tinygltf::Model &model,
	tinygltf::Primitive &primitive,
	VertexAttrib attrib
){
	const char *attrib_key = attrib_to_key(attrib);
	if(attrib_key == nullptr){
		spdlog::warn("Requested attribute '{}' not found in primitive");
		VertexTransformer::AttributeReadBufferInfo ret;
		ret.buffer = nullptr;
		return ret;
	}
	const tinygltf::Accessor &accessor = model.accessors[primitive.attributes[attrib_key]];
	return FromGLTFAccessor(model, accessor, attrib);
}