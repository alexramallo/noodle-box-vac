/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#include <string>
#include <sstream>
#include <fstream>
#include <span>
#include <spdlog/spdlog.h>

//TODO: don't do this...
#include "pspgu_defs.h"

#include <flatbuffers/flatbuffers.h>
#include "FlatbufferEncoder.h"
#include "PSPMesh_generated.h"

using namespace flatbuffers;
using namespace Engine::Graphics;

static const uint32_t VTYPE_ERR = 0;
static const uint32_t PTYPE_ERR = 69420101;

int FlatbufferEncoder::EncodeTransformer(
	std::vector<VertexTransformer> &trns,
	std::string destination,
	std::string name
){
	spdlog::info("Encoding mesh (parts: {}) \"{}\" to {}", trns.size(), name, destination);
	flatbuffers::FlatBufferBuilder builder(1024);

	//Parts
	std::vector<flatbuffers::Offset<Engine::Graphics::PSPMeshPart>> parts;
	for(int i = 0; i < trns.size(); i++){
		VertexTransformer &t = trns[i];
		spdlog::trace("part[{}] indices: {}, vertices: {}", i, t.exc_indices.size(), t.out_vertices_interleaved.size());
		
		int vtype = get_vtype(t);
		if(vtype == VTYPE_ERR){
			spdlog::critical("Invalid vtype {}, can't continue!", vtype);
			exit(1);
		}

		
		int ptype = get_ptype(t);
		if(ptype == PTYPE_ERR){
			spdlog::critical("Invalid ptype {}, can't continue!", ptype);
			exit(1);
		}

		int material = 0; //TODO

		builder.ForceVectorAlignment(t.exc_indices.size(), sizeof(uint8_t), 16);
		auto indices = builder.CreateVector(t.exc_indices);

		builder.ForceVectorAlignment(t.out_vertices_interleaved.size(), sizeof(uint8_t), 16);
		auto vertices = builder.CreateVector(t.out_vertices_interleaved);

		int num_elements;
		if(t.num_indices == 0){
			num_elements = t.num_vertices;
		}else{
			num_elements = t.num_indices;
		}

		assert(vtype < std::numeric_limits<uint32_t>::max());
		assert(ptype < std::numeric_limits<uint8_t>::max());
		assert(material < std::numeric_limits<uint8_t>::max());

		auto part = Engine::Graphics::CreatePSPMeshPart(
			builder,
	 		material,
			ptype,
			vtype,
			num_elements,
			t.vertex_stride,
			indices,
			vertices
		);

		parts.push_back(part);
	}
	auto vparts = builder.CreateVector(parts);
	//Materials (TODO)
	std::vector<uint8_t> materials(0);
	auto vmats = builder.CreateVector(materials);
	auto sname = builder.CreateString(name);
	auto fbmesh = CreatePSPMesh(builder, sname, vmats, vparts);

	builder.Finish(fbmesh);

	uint8_t *buf = builder.GetBufferPointer();
	int size = builder.GetSize();

	spdlog::trace("Writing {} bytes to {}", size, destination);
	std::ofstream out(destination);
	for(int i = 0; i < size; i++){
		out << buf[i];
	}
	out.close();
	return 0;
}

uint32_t FlatbufferEncoder::get_ptype(VertexTransformer &trns){
	assert(trns.mesh != nullptr);
	auto src_prim = trns.mesh->primitives[trns.prim_idx];
	switch(src_prim.mode){
		case POINTS:
			spdlog::trace("ptype: POINTS");
			return GU_POINTS;
		case LINES:
			spdlog::trace("ptype: LINES");
			return GU_LINES;
		case LINE_STRIP:
			spdlog::trace("ptype: LINE_STRIP");
			return GU_LINE_STRIP;
		case TRIANGLES:
			spdlog::trace("ptype: TRIANGLES");
			return GU_TRIANGLES;
		case TRIANGLE_STRIP:
			spdlog::trace("ptype: TRIANGLE_STRIP");
			return GU_TRIANGLE_STRIP;
		case TRIANGLE_FAN:
			spdlog::trace("ptype: FAN");
			return GU_TRIANGLE_FAN;
		default: //LINE_LOOP
			spdlog::critical("primitive type ({}) is not supported", src_prim.mode);
			return PTYPE_ERR;
	}
}

uint32_t FlatbufferEncoder::get_vtype(VertexTransformer &trns){
	uint32_t vtype = 0;
	//Indices
	switch(int dt = get_attribute_type(trns.output_format, VA_INDEX)){
		case DT_UBYTE:
			spdlog::trace("vtype: INDEX_8BIT");
			vtype |= GU_INDEX_8BIT;
			break;
		case DT_USHORT:
			spdlog::trace("vtype: INDEX_16BIT");
			vtype |= GU_INDEX_16BIT;
			break;
		default:
			spdlog::critical("Invalid data type {} for attribute INDEX", dt);
			return VTYPE_ERR;
	}

	//Position
	switch(int dt = get_attribute_type(trns.output_format, VA_POSITION)){
		case DT_BYTE:
			spdlog::trace("vtype: VERTEX_8BIT");
			vtype |= GU_VERTEX_8BIT;
			break;
		case DT_SHORT:
			spdlog::trace("vtype: VERTEX_16BIT");
			vtype |= GU_VERTEX_16BIT;
			break;
		case DT_FLOAT:
			spdlog::trace("vtype: VERTEX_32BITF");
			vtype |= GU_VERTEX_32BITF;
			break;
		default:
			spdlog::critical("Invalid data type ({}) for attribute POSITION", dt);
			return VTYPE_ERR;
	}

	//Normal
	switch(int dt = get_attribute_type(trns.output_format, VA_NORMAL)){
		case DT_NONE:
			break;
		case DT_BYTE:
			spdlog::trace("vtype: NORMAL_8BIT");
			vtype |= GU_NORMAL_8BIT;
			break;
		case DT_SHORT:
			spdlog::trace("vtype: NORMAL_16BIT");
			vtype |= GU_NORMAL_16BIT;
			break;
		case DT_FLOAT:
			spdlog::trace("vtype: NORMAL_32BITF");
			vtype |= GU_NORMAL_32BITF;
			break;
		default:
			spdlog::critical("Invalid data type {} for attribute NORMAL", dt);
			return VTYPE_ERR;
	}

	//Color
	switch(int dt = get_attribute_type(trns.output_format, VA_COLOR)){
		case DT_COL_NONE:
			break;
		case DT_COL_5650:
			spdlog::trace("vtype: COLOR_5650");
			vtype |= GU_COLOR_5650;
			break;
		case DT_COL_5551:
			spdlog::trace("vtype: COLOR_5551");
			vtype |= GU_COLOR_5551;
			break;
		case DT_COL_4444:
			spdlog::trace("vtype: COLOR_4444");
			vtype |= GU_COLOR_4444;
			break;
		case DT_COL_8888:
			spdlog::trace("vtype: COLOR_8888");
			vtype |= GU_COLOR_8888;
			break;
		default:
			spdlog::critical("Invalid data type {} for attribute COLOR", dt);
			return VTYPE_ERR;
	}

	//Texture
	switch(int dt = get_attribute_type(trns.output_format, VA_TEXTURE)){
		case DT_NONE:
			break;
		case DT_BYTE:
			spdlog::trace("vtype: TEXTURE_8BIT");
			vtype |= GU_TEXTURE_8BIT;
			break;
		case DT_SHORT:
			spdlog::trace("vtype: TEXTURE_16BIT");
			vtype |= GU_TEXTURE_16BIT;
			break;
		case DT_FLOAT:
			spdlog::trace("vtype: TEXTURE_32BITF");
			vtype |= GU_TEXTURE_32BITF;
			break;
		default:
			spdlog::critical("Invalid data type {} for attribute TEXTURE", dt);
			return VTYPE_ERR;
	}

	//Weights
	int num_weights = num_vertex_attribute_components(trns.output_format, VA_WEIGHTS);
	switch(int dt = get_attribute_type(trns.output_format, VA_WEIGHTS)){
		case DT_NONE:
			break;
		case DT_BYTE:
			spdlog::trace("vtype: WEIGHT_8BIT [{}]", num_weights);
			vtype |= GU_WEIGHT_8BIT;
			vtype |= GU_WEIGHTS(num_weights);
			break;
		case DT_SHORT:
			spdlog::trace("vtype: WEIGHT_16BIT [{}]", num_weights);
			vtype |= GU_WEIGHT_16BIT;
			vtype |= GU_WEIGHTS(num_weights);
			break;
		case DT_FLOAT:
			spdlog::trace("vtype: WEIGHT_32BITF [{}]", num_weights);
			vtype |= GU_WEIGHT_32BITF;
			vtype |= GU_WEIGHTS(num_weights);
			break;
		default:
			spdlog::critical("Invalid data type {} for attribute WEIGHT", dt);
			return VTYPE_ERR;
	}
	return vtype;
}