/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#ifndef ___FLATBUFFERENCODER__H___
#define ___FLATBUFFERENCODER__H___

#include "util/VertexTransformer.h"
#include "PSPMesh_generated.h"

class FlatbufferEncoder {
public:
	int EncodeTransformer(
		std::vector<VertexTransformer> &transformers,
		std::string destination,
		std::string name
	);

	uint32_t get_vtype(VertexTransformer &transformer);
	uint32_t get_ptype(VertexTransformer &transformer);
};

#endif //___FLATBUFFERENCODER__H___
