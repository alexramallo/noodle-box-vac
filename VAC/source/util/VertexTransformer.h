/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#ifndef __VERTEX_TRANSFORMER__H__
#define __VERTEX_TRANSFORMER__H__
#include <tiny_gltf.h>
#include <vector>

#include "VertexTypes.h"
#include "PSPMesh_generated.h"

class VertexTransformer {
	public:
		VertexTransformer();
		VertexTransformer(tinygltf::Mesh *mesh);
		VertexTransformer(tinygltf::Mesh *mesh, unsigned int output_format);
		~VertexTransformer() = default;

		struct AttributeReadBufferInfo {
			uint32_t format;
			const uint8_t *buffer;
			size_t buffer_length;
			size_t count;
			static AttributeReadBufferInfo FromGLTFPrimitive(tinygltf::Model &model, tinygltf::Primitive &primitive, VertexAttrib attrib);
			static AttributeReadBufferInfo FromGLTFPrimitive(tinygltf::Model &model, tinygltf::Primitive &primitive, std::string attrib);
			static AttributeReadBufferInfo FromGLTFAccessor(tinygltf::Model &model, const tinygltf::Accessor &accessor, VertexAttrib attrib);
		};

		struct AttribReadBufferSet {
			union {
				AttributeReadBufferInfo *buffers[6];
				struct {
					AttributeReadBufferInfo *position;
					AttributeReadBufferInfo *normal;
					AttributeReadBufferInfo *color;
					AttributeReadBufferInfo *texture;
					AttributeReadBufferInfo *weights;
					AttributeReadBufferInfo *indices;
				};
			};

			inline AttribReadBufferSet():
				buffers {nullptr, nullptr, nullptr, nullptr, nullptr, nullptr}
			{
				//--
			}
		};

		/**
			Will populate read input buffers and write them into `out_vertices` and
			`out_indices` based on the `output_format`
			from a given GLTF node/primitive
		*/
		int CreateIndexedVertexBufferFromPrim(AttribReadBufferSet &input_set);

	protected:
		int do_transformation(AttribReadBufferSet &input_set);

		/**
			Reads input buffers and writes them into separate buffers
			as `exc_*`, converted according to `output_format`
		*/
		int do_extraction(AttribReadBufferSet &input_set);
		
		/**
			Interleaves attributees based on output format
			MUST call `do_extraction` first
		*/
		int do_interleave_attributes(AttribReadBufferSet &input_set);

		int do_align_buffer(std::vector<uint8_t> &buf, size_t element_size, size_t element_count, size_t alignment);

	public:
		tinygltf::Mesh *mesh;
		int prim_idx;
		uint32_t num_vertices, num_indices, vertex_stride;
		std::vector<uint8_t> out_vertices_interleaved;

		std::vector<uint8_t> exc_indices;
		std::vector<uint8_t> exc_position;
		std::vector<uint8_t> exc_normal;
		std::vector<uint8_t> exc_color;
		std::vector<uint8_t> exc_texture;
		std::vector<uint8_t> exc_weights;
		unsigned int output_format;
};

#endif //__VERTEX_TRANSFORMER__H__