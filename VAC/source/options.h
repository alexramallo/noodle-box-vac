/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#ifndef __OPTIONS__H__
#define __OPTIONS__H__
#include <structopt/app.hpp>
#include <spdlog/spdlog.h>
#include "util/VertexTypes.h"

enum OptType {
	NONE = 0,
	FLOAT,
	SHORT,
	BYTE,
	INT,
	USHORT,
	UBYTE,
	UINT,

	//Color-specific
	C5650,
	C5551,
	C4444,
	C8888
};

struct Options {
	struct ExtractMesh : structopt::sub_command {
		//folder where to write outputs
		std::string output_directory;

		/**
			Output will be `output_name.pspmesh`

				* If multiple meshes match name_regex, then output will
				  be `output_name.1.pspmesh`, `output_name.2.pspmesh`, etc

			If not provided, output name will be GLTF mesh name with .pspmesh
			extension
		*/
		std::optional<std::string> output_name;

		std::optional<OptType>  Indices  = USHORT;
		std::optional<OptType>  Position = FLOAT;
		std::optional<OptType>  Normal   = FLOAT;
		std::optional<OptType>	Color    = C8888;
		std::optional<OptType>  Texture  = FLOAT;
		std::optional<OptType>  Weights  = FLOAT;

		//Regex for filtering meshes based on their names
		std::optional<std::string> name_regex = ".*";

		//if true, will only print output filenames and not do anything else
		std::optional<bool> DryRun = false;

		//If true, existing files will be overwritten
		std::optional<bool> AllowOverwrite = false;

		//List of GLTF files to process
		std::vector<std::string> files;
	};
	ExtractMesh extract_mesh;

	struct ExtractScene: structopt::sub_command {
		std::string output_directory;
		std::vector<std::string> files;
	};
	ExtractScene extract_scene;


	std::optional<spdlog::level::level_enum> LogLevel = spdlog::level::level_enum::warn;
};
STRUCTOPT(Options::ExtractMesh,
	output_directory,
	output_name,
	Indices,
	Position,
	Normal,
	Color,
	Texture,
	Weights,
	name_regex,
	DryRun,
	AllowOverwrite,
	files
);
STRUCTOPT(Options::ExtractScene,
	output_directory,
	files
);
STRUCTOPT(Options, extract_mesh, extract_scene, LogLevel);

#endif //__OPTIONS__H__