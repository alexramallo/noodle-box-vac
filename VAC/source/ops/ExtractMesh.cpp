/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/

#include <tiny_gltf.h>
#include <spdlog/spdlog.h>
#include <re2/re2.h>
#include <fstream>

#include "ExtractMesh.h"
#include "util/VertexTransformer.h"
#include "util/VertexTypes.h"
#include "util/FlatbufferEncoder.h"

using namespace tinygltf;

static int process_model(Model &model, const Options &options);

VDataType DT_From_OptType(OptType o){
	switch(o){
		case FLOAT: return DT_FLOAT;
		case SHORT: return DT_SHORT;
		case BYTE: return DT_BYTE;
		case INT: return DT_INT;
		case USHORT: return DT_USHORT;
		case UBYTE: return DT_UBYTE;
		case UINT: return DT_UINT;
		default: return DT_NONE;
	}
}

VDataType DT_From_OptCol(OptType o){
	switch(o){
		case C5650: return DT_COL_5650;
		case C5551: return DT_COL_5551;
		case C4444: return DT_COL_4444;
		case C8888: return DT_COL_8888;
		default: 	return DT_COL_NONE;
	}
}

unsigned int get_output_format_from_options(const Options &options){
	auto opts = options.extract_mesh;
	//TODO: return output format based on options

	spdlog::trace("Options:\n\tindices: {}\n\tpos: {}\n\tnormal: {}\n\tcolor: {}\n\ttexture: {}\n\tweights: {}",
		opts.Indices.value(),
		opts.Position.value(),
		opts.Normal.value(),
		opts.Color.value(),
		opts.Texture.value(),
		opts.Weights.value()
	);

	unsigned int ret = 0;
	ret |= DT_From_OptType(opts.Indices.value())	<< VA_INDEX;
	ret |= DT_From_OptType(opts.Position.value())	<< VA_POSITION;
	ret |= DT_From_OptType(opts.Normal.value()) 	<< VA_NORMAL;
	ret |= DT_From_OptCol(opts.Color.value()) 		<< VA_COLOR;
	ret |= DT_From_OptType(opts.Texture.value()) 	<< VA_TEXTURE;
	ret |= DT_From_OptType(opts.Weights.value()) 	<< VA_WEIGHTS;
	return ret;
	
	//return VT_SKINNED_TEXTURED_LIT_MESH | INDEX_UINT | COLOR_8888;
	//return INDEX_UINT | (POSITION_BYTE | COLOR_5551);
	//return POSITION_FLOAT | COLOR_FLOAT_RGBA | INDEX_UBYTE;
}

int ExtractMesh::run(const Options &options){
	auto opts = options.extract_mesh;

	for(std::string &file: opts.files){
		spdlog::info("Processing file: {}", file);
		Model model;
		TinyGLTF loader;
		std::string err;
		std::string warn;
		if(file.find(".glb") != std::string::npos){
			spdlog::trace("parsing binary GLTF");
			if(!loader.LoadBinaryFromFile(&model, &err, &warn, file)){
				spdlog::error("Failed to parse {}", file);
				return 1;
			}
		}else if(file.find(".gltf") != std::string::npos){
			spdlog::trace("parsing text GLTF");			
			if(!loader.LoadASCIIFromFile(&model, &err, &warn, file)){
				spdlog::error("Failed to parse {}", file);
				return 1;
			}
		}
		if(!warn.empty()){
			spdlog::warn("tinygltf: {}", warn);
		}
		if(!err.empty()){
			spdlog::error("tinygltf: {}", err);
		}

		unsigned int out_fmt = get_output_format_from_options(options);

		RE2 pattern_name(opts.name_regex.value());

		spdlog::info("Scene count: {}, Node count: {}, Mesh count: {}",
			model.scenes.size(),
			model.nodes.size(),
			model.meshes.size()
		);

		int extract_count = 0;
		std::map<std::string, int> dupe_name_ct;

		std::vector<std::string> outputs;

		for(Mesh &mesh: model.meshes){
			spdlog::trace("Processing mesh {}", mesh.name);

			//Skip meshes that don't match the name pattern option
			if(!RE2::FullMatch(mesh.name, pattern_name)){
				spdlog::trace("Skipping mesh \"{}\" because doesn't match name regex", mesh.name);
				continue;
			}

			std::string outname = mesh.name;
			if(opts.output_name.has_value()){
				outname = opts.output_name.value();
			}
			if(dupe_name_ct.find(outname) != dupe_name_ct.end()){
				int ct = dupe_name_ct[outname]++;
				outname += std::string(".") + std::to_string(ct);
			}else{
				dupe_name_ct[outname] = 1;
			}
			std::string dst = opts.output_directory + "/" + outname + ".pspmesh";

			if(opts.DryRun.value()){
				assert(0);
				outputs.push_back(dst);
				extract_count++;
				continue;
			}

			//Check if output file already exists
			if(!opts.AllowOverwrite.value()){
				std::ifstream chk(dst);
				if(chk.good()){
					spdlog::error("Output file already exists: {}", dst);
					return 1;
				}
				chk.close();
			}

			spdlog::trace("\tMesh has {} primitives", mesh.primitives.size());

			std::vector<VertexTransformer> transformers(0);
			transformers.resize(mesh.primitives.size());
			for(int p = 0; p < mesh.primitives.size(); p++){
				new(&transformers[p]) VertexTransformer(&mesh, out_fmt);
				process_primitive(transformers[p], model, mesh, p, options);
			}

			FlatbufferEncoder enc;
			if(enc.EncodeTransformer(transformers, dst, mesh.name)){
				spdlog::error("Failed to write flatbuffer to {}", dst);
			}else{
				extract_count++;
				outputs.push_back(dst);
			}
		}

		spdlog::info("Final extract count: {}", extract_count);

		for(auto &out: outputs){
			std::cout << "OUT: " << out << std::endl;
		}
	}

	return 0;
}

/**
	A 'node' in GLTF may only have one mesh, but each mesh may define
	more than one 'primitive'

	A primitive is collection of vertex attributes/indices and one material

	If a mesh has multiple primitives, then we split it into separate
	output fbufs
*/
int ExtractMesh::process_primitive(VertexTransformer &transformer, Model &model, Mesh &mesh, int p_idx, const Options &options){
	auto opts = options.extract_mesh;
	spdlog::info("process_primitive(mesh: {}, p_idx: {})", mesh.name, p_idx);

	Primitive &prim = mesh.primitives[p_idx];
	transformer.prim_idx = p_idx;

	std::vector<VertexTransformer::AttributeReadBufferInfo> input_buffers(6);
	VertexTransformer::AttribReadBufferSet input_set;

	if(prim.attributes.find("POSITION") != prim.attributes.end()){
		input_buffers[0] = VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(model, prim, "POSITION");
		input_set.position = &input_buffers[0];
	}

	if(prim.attributes.find("NORMAL") != prim.attributes.end()){
		input_buffers[1] = VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(model, prim, "NORMAL");
		input_set.normal = &input_buffers[1];
	}

	if(prim.attributes.find("COLOR_0") != prim.attributes.end()){
		input_buffers[2] = VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(model, prim, "COLOR_0");
		input_set.color = &input_buffers[2];
	}

	if(prim.attributes.find("TEXCOORD_0") != prim.attributes.end()){
		input_buffers[3] = VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(model, prim, "TEXCOORD_0");
		input_set.texture = &input_buffers[3];
	}

	/* Ignore weights for now
	if(prim.attributes.find("WEIGHTS_0") != prim.attributes.end()){
		input_buffers[4] = VertexTransformer::AttributeReadBufferInfo::FromGLTFPrimitive(model, prim, "WEIGHTS_0");
		input_set.weights = &input_buffers[4];
	}
	*/

	if(prim.indices >= 0){
		const Accessor &accessor = model.accessors[prim.indices];
		input_buffers[5] = VertexTransformer::AttributeReadBufferInfo::FromGLTFAccessor(model, accessor, VA_INDEX);
		input_set.indices = &input_buffers[5];
	}

	int res = transformer.CreateIndexedVertexBufferFromPrim(input_set);
	if(res){
		spdlog::error("CreateIndexedVertexBufferFromPrim failed with result {}", res);
		return res;
	}else{
		spdlog::info("Success!");
	}
	return 0;
}