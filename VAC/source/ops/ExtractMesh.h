/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#ifndef __EXTRACT_STATIC_MESH__H__
#define __EXTRACT_STATIC_MESH__H__

#include <structopt/app.hpp>
#include "Operation.h"
#include "options.h"

#include "util/VertexTransformer.h"

class ExtractMesh: public Operation {
public:
	int run(const Options &options);
	int process_primitive(VertexTransformer &transformer, tinygltf::Model &model, tinygltf::Mesh &mesh, int p_idx, const Options &options);
};

#endif //__EXTRACT_STATIC_MESH__H__