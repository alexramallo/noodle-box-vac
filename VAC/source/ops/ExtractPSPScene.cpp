/*
Copyright (C) 2022 Alejandro Ramallo (alejandro@ramallo.me)

This file is part of VAC.

VAC is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

VAC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
VAC. If not, see <https://www.gnu.org/licenses/>. 
*/
#include <tiny_gltf.h>
#include <spdlog/spdlog.h>
#include <re2/re2.h>
#include <fstream>
#include <string>
#include <sstream>

#include <glm/ext.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/vec3.hpp>

#include <flatbuffers/flatbuffers.h>
#include "PSPScene_generated.h"

#include "ExtractPSPScene.h"
#include "util/VertexTransformer.h"
#include "util/VertexTypes.h"
#include "util/FlatbufferEncoder.h"

using namespace tinygltf;
using namespace flatbuffers;
using namespace Engine::Graphics;

int ExtractPSPScene::run(const Options &options){
	auto opts = options.extract_scene;

	spdlog::trace("ExtractPSPScene! ({} files)", opts.files.size());

	for(std::string &file: opts.files){
		spdlog::info("Processing file: {}", file);
		Model model;
		TinyGLTF loader;
		std::string err;
		std::string warn;
		if(file.find(".glb") != std::string::npos){
			spdlog::trace("parsing binary GLTF");
			if(!loader.LoadBinaryFromFile(&model, &err, &warn, file)){
				spdlog::error("Failed to parse {}", file);
				return 1;
			}
		}else if(file.find(".gltf") != std::string::npos){
			spdlog::trace("parsing text GLTF");			
			if(!loader.LoadASCIIFromFile(&model, &err, &warn, file)){
				spdlog::error("Failed to parse {}", file);
				return 1;
			}
		}
		if(!warn.empty()){
			spdlog::warn("tinygltf: {}", warn);
		}
		if(!err.empty()){
			spdlog::error("tinygltf: {}", err);
		}

		spdlog::info("Scene count: {}, Node count: {}, Mesh count: {}",
			model.scenes.size(),
			model.nodes.size(),
			model.meshes.size()
		);

		int extract_count = 0;
		std::map<std::string, int> dupe_name_ct;
		std::vector<std::string> outputs;

		for(int i = 0; i < model.scenes.size(); i++){
			std::string outname = model.scenes[i].name;
			std::string dst = opts.output_directory + "/" + outname + ".pspscene";
			if(process_scene(model, i, dst)){
				spdlog::error("Error while processing scene {} / {}", i, model.scenes.size());
			}else{
				outputs.push_back(dst);
			}
		}

		for(auto &out: outputs){
			std::cout << "OUT: " << out << std::endl;
		}
	}

	return 0;
}

int ExtractPSPScene::process_scene(tinygltf::Model &model, int scene_idx, std::string dst){
	spdlog::trace("process_scene[{}]", scene_idx);
	Scene &scene = model.scenes[scene_idx];

	FlatBufferBuilder builder(1024);
	
	std::vector<Offset<PSPCameraNode>> camera_nodes;
	std::vector<Offset<PSPMeshNode>> mesh_nodes;
	std::vector<Offset<PSPSceneNode>> empty_nodes;

	std::map<std::string, int> meshes;
	std::vector<Offset<flatbuffers::String>> mesh_names;

	for(int i = 0; i < scene.nodes.size(); i++){
		Node &node = model.nodes[scene.nodes[i]];

		Vec3 translation = Vec3(0,0,0);
		Vec3 scale = Vec3(1, 1, 1);
		Vec3 rotation = Vec3(0, 0, 0);

		if(node.rotation.size() == 4){
			//Convert gltf quaternion rotation to euler angles as expected by sceGum
			glm::quat quaternion;
			quaternion.x = node.rotation[0];
			quaternion.y = node.rotation[1];
			quaternion.z = node.rotation[2];
			quaternion.w = node.rotation[3];
			glm::vec3 euler(glm::eulerAngles(quaternion));
			rotation = Vec3(
				euler.x,
				euler.y,
				euler.z
			);
		}

		if(node.translation.size() == 3){
			translation = Vec3(node.translation[0], node.translation[1], node.translation[2]);
		}

		if(node.scale.size() == 3){
			scale = Vec3(node.scale[0], node.scale[1], node.scale[2]);
		}

		const Transform transform{
			translation,
			rotation,
			scale
		};

		spdlog::trace("\n\t[{}]\n\ttranslation: ({}, {}, {})\n\tscale: ({}, {}, {})\n\teulerAngles: ({}, {}, {})",
			node.name,
			translation.x(), translation.y(), translation.z(),
			scale.x(), scale.y(), scale.z(),
			rotation.x(), rotation.y(), rotation.z()
		);

		if(node.camera >= 0){
			//add camera
			Camera &cam = model.cameras[node.camera];
			if(cam.type != "perspective"){
				spdlog::warn("Skipping camera {} because it is of unsupported: type {}", cam.name, cam.type);
				continue;
			}

			//sceGumPerspective expects degrees
			float yfov = glm::degrees(cam.perspective.yfov);

			camera_nodes.push_back(CreatePSPCameraNode(
				builder,
				builder.CreateString(node.name),
				&transform,
				(float)cam.perspective.aspectRatio,
				(float)yfov,
				(float)cam.perspective.zfar,
				(float)cam.perspective.znear
			));
		}

		if(node.mesh >= 0){
			//add mesh
			Mesh gltf_mesh = model.meshes[node.mesh];
			if(meshes.find(gltf_mesh.name) == meshes.end()){
				meshes[gltf_mesh.name] = mesh_names.size();
				mesh_names.push_back(builder.CreateString(gltf_mesh.name));
			}
			mesh_nodes.push_back(CreatePSPMeshNode(
				builder,
				builder.CreateString(node.name),
				&transform,
				meshes[gltf_mesh.name]
			));
		}else{
			empty_nodes.push_back(CreatePSPSceneNode(
				builder,
				builder.CreateString(node.name),
				&transform
			));
		}
	}

	auto pspscene = CreatePSPScene(
		builder,
		builder.CreateString(scene.name),
		builder.CreateVector(empty_nodes),
		builder.CreateVector(mesh_nodes),
		builder.CreateVector(camera_nodes),
		builder.CreateVector(mesh_names)
	);

	builder.Finish(pspscene);

	uint8_t *buf = builder.GetBufferPointer();
	int size = builder.GetSize();

	spdlog::trace("Writing {} bytes to {}", size, dst);
	std::ofstream out(dst);
	for(int i = 0; i < size; i++){
		out << buf[i];
	}
	out.close();

	return 0;
}