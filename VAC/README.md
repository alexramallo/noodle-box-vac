# Vertex Attribute Converter (V.A.C.)

This is an offline tool for converting vertex attribute data to different formats.

It is pre-pre-alpha, buggy, and probably slow/inefficient. It's also designed for the needs of
a single project, and likely a pain to use for anything else. Proceed at your own risk!