cmake_minimum_required(VERSION 3.15 FATAL_ERROR)
project(vac VERSION 0.1.0 LANGUAGES CXX)

find_package(glm CONFIG REQUIRED)
find_package(re2 CONFIG REQUIRED)
find_package(fmt CONFIG REQUIRED)
find_package(spdlog CONFIG REQUIRED)
find_package(tinygltf CONFIG REQUIRED)
find_package(structopt CONFIG REQUIRED)
find_package(Flatbuffers CONFIG REQUIRED)

set(DEP_TARGETS
	glm::glm
	re2::re2
	fmt::fmt
	spdlog::spdlog
	tinygltf::tinygltf
	structopt::structopt
	flatbuffers::flatbuffers
)

set(SOURCES 
	${CMAKE_SOURCE_DIR}/source/main.cpp
	${CMAKE_SOURCE_DIR}/source/tinygltf.cpp
	${CMAKE_SOURCE_DIR}/source/ops/ExtractMesh.cpp
	${CMAKE_SOURCE_DIR}/source/ops/ExtractPSPScene.cpp
	${CMAKE_SOURCE_DIR}/source/util/FlatbufferEncoder.cpp
	${CMAKE_SOURCE_DIR}/source/util/VertexTransformer.cpp
	${CMAKE_SOURCE_DIR}/source/util/AttributeReadBufferInfo.cpp
)

set(INCLUDES
	${CMAKE_SOURCE_DIR}/source
	${CMAKE_SOURCE_DIR}/source/ops
	${CMAKE_SOURCE_DIR}/source/util
)

set(SCHEMAS
	${CMAKE_SOURCE_DIR}/schema/PSPMesh.fbs
	${CMAKE_SOURCE_DIR}/schema/PSPScene.fbs
)

flatbuffers_generate_headers(
	TARGET FBUFS
	SCHEMAS ${SCHEMAS}
)

add_executable(vac ${SOURCES})
include_directories(${INCLUDES})
target_link_libraries(vac ${DEP_TARGETS} FBUFS)

install(
	TARGETS vac
)
install(
	FILES
		${CMAKE_BINARY_DIR}/FBUFS/PSPScene_generated.h
		${CMAKE_BINARY_DIR}/FBUFS/PSPMesh_generated.h
	DESTINATION include
)
install(
	FILES
		${SCHEMAS}
	DESTINATION schema
)
install(
	FILES
		${CMAKE_SOURCE_DIR}/waftools/vac.py
	DESTINATION waftools
)
