#!/bin/sh

#RUN THIS FROM THE PROJECT ROOT ONLY!!

####################
# Compile VAC
####################
cd VAC
mkdir -p build
cd build

#Download and/or compile dependencies
conan install .. -pr:b=default -pr:h=default --build=missing

#compile
CMAKE_PREFIX_PATH=. cmake .. -DCMAKE_BUILD_TYPE=Release
make

#Return to project root
cd ../../
####################


####################
# Export custom flatbuffers recipe to Conan cache
# (includes pspsdk compatibility tweaks)
####################
conan export ./conan_flatbuffers/conanfile.py flatbuffers/2.0.0@psp/dev
####################


####################
# Build the game/demo
####################
mkdir -p build
cd build

#Install dependencies (just flatbuffers)
conan install .. -pr:h=../pspdev.jinja -pr:b=default --build=missing -sbuild_type=Release

#Compile code
CMAKE_PREFIX_PATH=. psp-cmake .. -DCMAKE_BUILD_TYPE=Release -DMAKE_PRX=1
make

#Prepare compiled assets folder
mkdir -p assets/warehouse.gltf
cp ../assets/{*.sfl,*.png} ./assets

#Compile gltf
export MESHARGS=--Color=C8888\ --Position=FLOAT\ --Indices=USHORT\ --Normal=NONE\ --Texture=NONE\ --Weights=NONE
../VAC/build/vac extract_mesh `echo $MESHARGS` assets/warehouse.gltf ../assets/warehouse.gltf
../VAC/build/vac extract_scene assets/warehouse.gltf ../assets/warehouse.gltf
####################


####################
# Copy artifacts to a folder
####################
cd ..
mkdir -p PSP/GAME/VACDEMO
mv build/assets PSP/GAME/VACDEMO/assets
mv build/EBOOT.PBP PSP/GAME/VACDEMO
echo "Copy the 'PSP' folder to the root of your PSP, or open it in an emulator"
####################