/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#include <stdlib.h>
#include <string>
#include <sstream>
#include <inttypes.h>

#include <pspkernel.h>
#include <pspdisplay.h>
#include <pspdebug.h>
#include <pspgu.h>
#include <pspgum.h>

#include <malloc.h>
#include <math.h>

#include "Game.h"
#include "Scene.h"

#include "Drawing2D.h"
using namespace Drawing2D;

#include "PSPMesh_generated.h"
using namespace Engine::Graphics; //defined in PSPMesh_generated.h

const PSPMesh *mesh = nullptr;

///////////
// Really shitty demo allocator
void *pSceneMem = nullptr;
size_t pSceneMem_head = 0;
size_t pSceneMem_end = 0;

void *scene_alloc(size_t s){
	while(pSceneMem_head % 16 != 0){
		pSceneMem_head++;
	}

	if(pSceneMem_head + s >= pSceneMem_end){
		return nullptr;
	}
	void *ret = pSceneMem + pSceneMem_head;
	pSceneMem_head += s;
	return ret;
}

void scene_release(void *p){
	//do nothing X_X
}

void scene_demolish(){
	pSceneMem_head = 0;
}
///////////

std::string load_text = "Press SELECT to load";

void* (*alloc_texture)(size_t) = malloc;
void (*release_texture)(void*) = free;

Font fnt_baloo;

void doLoadScene(Scene **out, const char *file){
	*out = new Scene(file);
	(*out)->alloc = scene_alloc;
	(*out)->release = scene_release;

	int bytes_start = pSceneMem_head;
	uint64_t load_start = sceKernelGetSystemTimeWide();
	(*out)->loadAll(false);
	uint64_t load_ms = (sceKernelGetSystemTimeWide() - load_start) / 1000;
	int num_bytes = pSceneMem_head - bytes_start;

	std::stringstream ss;
	ss << "Loaded " << num_bytes << " bytes in " << load_ms << "ms (START to unload)";
	load_text = ss.str();
	printf("Loaded scene \"%s\" in %" PRIu64 " ms (allocated %u / %d bytes)\n", file, load_ms, pSceneMem_head, pSceneMem_end);
}

void doUnloadScene(Scene **out){
	printf("Unloaded scene\n");
	(*out)->unloadAll();
	scene_demolish();
	free(*out);
	*out = nullptr;
}

int Game::boot(int argc, char *argv[]){
	sceCtrlSetSamplingCycle(0);
	sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);

	init_display();

	pSceneMem_end = 1 * 1024 * 1024; //this is overkill
	pSceneMem = aligned_alloc(16, pSceneMem_end);
	assert(pSceneMem != nullptr);

	size_t vertbuf_len = 16 * 1024;
	uint8_t *vertbuf = (uint8_t*) aligned_alloc(16, vertbuf_len);
	InitDrawing(vertbuf, vertbuf_len);

	fnt_baloo = loadFont("assets/baloo_regular_32.sfl", 32);
	assert(fnt_baloo.data != nullptr && "Failed to load font!");


	void *pScene = scene_alloc(sizeof(Scene));
	assert(pScene != nullptr);

	doLoadScene(&scWarehouse, "assets/warehouse.gltf/Scene.pspscene");

	sceKernelDcacheWritebackAll();

	cam_pos.x = -61.0f;
	cam_pos.y = -8.0f;
	cam_pos.z = -142.0f;

	unsigned int last = clock();
	while(1){
		sceCtrlReadBufferPositive(&pad, 1);
		unsigned int cur = clock();
		float elapsed = (float)((cur - last)/1000000.0f);
		last = cur;
		tick(elapsed);
	}
	return 0;
}

ScePspFVector4 multVector(ScePspFVector4 &a, ScePspFVector4 &b){
	return {a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w};
}
ScePspFVector3 multVector(ScePspFVector3 &a, ScePspFVector3 &b){
	return {a.x * b.x, a.y * b.y, a.z * b.z};
}
ScePspFVector4 multVector(ScePspFVector4 &a, float b){
	return {a.x * b, a.y * b, a.z * b, a.w * b};
}
ScePspFVector3 multVector(ScePspFVector3 &a, float b){
	return {a.x * b, a.y * b, a.z * b};
}
ScePspFVector3 addVector(ScePspFVector3 &a, ScePspFVector3 &b){
	return {a.x + b.x, a.y + b.y, a.z + b.z};
}
ScePspFVector3 addVector(ScePspFVector3 &a, ScePspFVector4 &b){
	return {a.x + b.x, a.y + b.y, a.z + b.z};
}

void *frambuffer = pDraw;

float cam_yaw = 0.0f;
float cam_pitch = 0.0f;

void Game::tick(float elapsed){
	static float val = 0;
	val+=1.0f;

	float turn_spd = 2.0f * elapsed;
	float move_spd = 2.0f * elapsed * (pad.Buttons & PSP_CTRL_RTRIGGER) ? 3.0f : 1.0f;

	if(pad.Buttons & PSP_CTRL_LEFT) cam_yaw   -= turn_spd;
	if(pad.Buttons & PSP_CTRL_RIGHT) cam_yaw  += turn_spd;
	if(pad.Buttons & PSP_CTRL_UP) cam_pitch   -= turn_spd;
	if(pad.Buttons & PSP_CTRL_DOWN) cam_pitch += turn_spd;

	float dz = 0.5f;
	float analog_x = ((float)(pad.Lx - 128) / 128.0f);
	float analog_y = ((float)(pad.Ly - 128) / 128.0f);
	if(analog_x > -dz && analog_x < dz) analog_x = 0.0f;
	if(analog_y > -dz && analog_y < dz) analog_y = 0.0f;

	cam_yaw += analog_x * turn_spd;
	cam_pitch += analog_y * turn_spd;

	sceGuStart(GU_DIRECT, dlist_main);
	sceGuClearColor(0xff554433);
	sceGuClearDepth(0);
	sceGuClear(GU_COLOR_BUFFER_BIT|GU_DEPTH_BUFFER_BIT);


	////////////////
	//Camera
	////////////////

	//scWarehouse->activateCamera(1);

	sceGumMatrixMode(GU_PROJECTION);
	sceGumLoadIdentity();
	sceGumPerspective(75.0f, (float)SCR_WIDTH/(float)SCR_HEIGHT, 3.0f, 500.0f);

	sceGumMatrixMode(GU_VIEW);

	cam_rot.x = cam_pitch;
	cam_rot.y = cam_yaw;
	cam_rot.z = 0.0f;

	gumLoadIdentity(&cam_mat);
	gumRotateXYZ(&cam_mat, &cam_rot);

	ScePspFVector3 cam_right = {cam_mat.x.x, cam_mat.y.x, cam_mat.z.x};
	ScePspFVector3 cam_fwd = {cam_mat.x.z, cam_mat.y.z, cam_mat.z.z};
	ScePspFVector3 cam_up = {cam_mat.x.y, cam_mat.y.y, cam_mat.z.y};

	if(pad.Buttons & PSP_CTRL_TRIANGLE){
		auto vec = pad.Buttons & PSP_CTRL_LTRIGGER ? multVector(cam_up, -1.0f) : cam_fwd;
		auto trans = multVector(vec, move_spd);
		cam_pos = addVector(cam_pos, trans);
	}

	if(pad.Buttons & PSP_CTRL_CROSS){
		auto vec = pad.Buttons & PSP_CTRL_LTRIGGER ? multVector(cam_up, -1.0f) : cam_fwd;
		auto trans = multVector(vec, -move_spd);
		cam_pos = addVector(cam_pos, trans);
	}

	if(pad.Buttons & PSP_CTRL_SQUARE){
		auto trans = multVector(cam_right, move_spd);
		cam_pos = addVector(cam_pos, trans);
	}
	if(pad.Buttons & PSP_CTRL_CIRCLE){
		auto trans = multVector(cam_right, -move_spd);
		cam_pos = addVector(cam_pos, trans);
	}

	gumTranslate(&cam_mat, &cam_pos);
	sceGumLoadMatrix(&cam_mat);

	////////////////


	if(scWarehouse != nullptr){
		sceGuDisable(GU_LIGHTING);
		sceGuDisable(GU_TEXTURE_2D);
		sceGuDisable(GU_BLEND);

		scWarehouse->drawAllMeshes();
		if(pad.Buttons & PSP_CTRL_START){
			doUnloadScene(&scWarehouse);
			load_text = "Press SELECT to load";
		}
	}else if(pad.Buttons & PSP_CTRL_SELECT){
		load_text = "Loading...";
	}

	//2d
	BeginDrawing();
	DrawRectangle(0, 0, SCR_WIDTH, 20, RGBA8(0, 0, 100, 255));
	DrawText(fnt_baloo, load_text.c_str(), 0, 16, COL_WHITE, 0.5, 0.5);
	EndDrawing(true);

	sceGuFinish();
	sceGuSync(0,0);

	sceDisplayWaitVblankStart();
	frambuffer = sceGuSwapBuffers();

	if(scWarehouse == nullptr && pad.Buttons & PSP_CTRL_SELECT){
		doLoadScene(&scWarehouse, "assets/warehouse.gltf/Scene.pspscene");
	}
}

void Game::drawMesh(const PSPMesh *mesh){
	for(int i = 0; i < mesh->parts()->size(); i++){
		auto part = mesh->parts()->Get(i);
		sceGumDrawArray(
			part->primitive(),
			part->vertex_format(),
			part->num_elements(),
			part->indices()->data(),
			part->vertices()->data()
		);
	}
}