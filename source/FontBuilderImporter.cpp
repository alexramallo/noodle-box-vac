/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#include "FontBuilderImporter.h"
#include <assert.h>

#include <string>
#include <stdlib.h>

using namespace FontBuilder;

enum LineReaderState {
	INVALID,
	COMPLETE,
	FontFamily,
	FontDimensions,
	Filename,
	GlyphCount,
	Glyph
	//KerningCount,
	//Kerning
};

class LineReader {
	FontData *output;
	int glyphIdx;
public:
	LineReaderState state;

	LineReader(FontData *o):
		output(o),
		state(FontFamily),
		glyphIdx(0)
	{
		//--
	}
	
	~LineReader()
	{
		//--
	}
	/**
		returns the next state
	*/
	LineReaderState readLine(const char *line, int length){
		char *p;

		assert(output != nullptr);

		switch(state){
			default: assert(false); return INVALID; //invalid state

			case FontFamily:
				output->family = std::string(std::string(line), 0, length);
				state = FontDimensions;
				break;

			case FontDimensions:
				output->size = strtol(&line[0], &p, 10);
				assert(p != nullptr);
				output->ascender = strtol(p, &p, 10);
				assert(p != nullptr);
				output->descender = strtol(p, &p, 10);
				assert(p != nullptr);
				output->lineHeight = strtol(p, &p, 10);
				assert(p != nullptr);
				state = Filename;
				break;

			case Filename:
				output->filename = std::string(std::string(line), 0, length);
				state = GlyphCount;
				break;
				
			case GlyphCount:
				output->numGlyphs = strtol(&line[0], nullptr, 10);
				state = Glyph;
				break;

			case Glyph:
				assert(glyphIdx < output->numGlyphs);

				int gID = strtol(&line[0], &p, 10);

				#if USE_DYNAMIC_GLYPH_STORAGE
					int g_slot = glyphIdx;
				#else
					int g_slot = gID;
				#endif
					
				assert(output->glyphs + g_slot != nullptr);

				
				output->glyphs[g_slot].ID = gID;
				assert(p != nullptr);
				output->glyphs[g_slot].PlaceX = strtol(p, &p, 10);
				assert(p != nullptr);
				output->glyphs[g_slot].PlaceY = strtol(p, &p, 10);
				assert(p != nullptr);
				output->glyphs[g_slot].PlaceW = strtol(p, &p, 10);
				assert(p != nullptr);
				output->glyphs[g_slot].PlaceH = strtol(p, &p, 10);
				assert(p != nullptr);
				output->glyphs[g_slot].OffsetX = strtol(p, &p, 10);
				assert(p != nullptr);
				output->glyphs[g_slot].OffsetY = strtol(p, &p, 10);
				assert(p != nullptr);
				output->glyphs[g_slot].AdvanceX = strtol(p, &p, 10);

				glyphIdx++;

				if(glyphIdx >= output->numGlyphs){
					state = COMPLETE;
				}
				break;
		}

		return state;
	}
};

FontData *FontBuilder::importFontDataEmbedded(const unsigned char *data, int length){
	FontData *out = new FontData();
	LineReader reader(out);

	unsigned char at;

	bool did_alloc_glyph = false;

	for(int l1 = 0, l2 = 0; l1 < length && reader.state != COMPLETE; l2++){
		if(l1 + l2 >= length){
			at = '\n';
		}else{
			at = data[l1 + l2];
		}

		if(at == '\n'){
			#if USE_DYNAMIC_GLYPH_STORAGE
				if(reader.readLine((const char*)&data[l1], l2) == Glyph && !did_alloc_glyph){
					out->glyphs = new GlyphData[out->numGlyphs]();
					did_alloc_glyph = true; 
				}
			#else
				reader.readLine((const char*)&data[l1], l2);
			#endif
			
			l1 += l2 + 1;
			l2 = 0;
		}
	}

	return out;
}

void FontBuilder::freeFontData(FontData *data){
	#if USE_DYNAMIC_GLYPH_STORAGE
		delete[] data->glyphs;
	#endif
	delete data;
}