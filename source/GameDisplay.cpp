/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#include <pspkernel.h>
#include <pspdisplay.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspge.h>

#include "Game.h"

int Game::init_display(){
	sceGuInit();

	sceGuStart(GU_DIRECT, dlist_main);
	sceGuDrawBuffer(DRAW_FMT, pDraw, BUF_WIDTH);
	sceGuDispBuffer(SCR_WIDTH, SCR_HEIGHT, pDisplay, BUF_WIDTH);
	sceGuDepthBuffer(pDepth, BUF_WIDTH);

	sceGuOffset(2048 - (SCR_WIDTH / 2), 2048 - (SCR_HEIGHT / 2));
	sceGuViewport(2048, 2048, SCR_WIDTH, SCR_HEIGHT);
	sceGuDepthRange(65535,0);
	
	sceGuScissor(0,0,SCR_WIDTH,SCR_HEIGHT);
	sceGuDepthFunc(GU_GEQUAL);
	sceGuFrontFace(GU_CCW);
	sceGuShadeModel(GU_SMOOTH);	

	sceGuEnable(GU_SCISSOR_TEST);
	sceGuEnable(GU_DEPTH_TEST);
	sceGuEnable(GU_CULL_FACE);
	sceGuEnable(GU_CLIP_PLANES);
	//sceGuEnable(GU_LIGHTING);
	//sceGuEnable(GU_LIGHT0);
	//sceGuEnable(GU_LIGHT1);
	//sceGuEnable(GU_LIGHT2);
	//sceGuEnable(GU_LIGHT3);

	sceGuDisable(GU_TEXTURE_2D);
	sceGuDisable(GU_BLEND);	

	sceGuFinish();
	sceGuSync(0,0);

	sceDisplayWaitVblankStart();
	sceGuDisplay(GU_TRUE);

	return 0;
}