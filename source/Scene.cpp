/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#include "Scene.h"
#include <sstream>
#include <stdio.h>
#include <assert.h>
#include <cfloat>
#include <cmath>
#include <cinttypes>
#include <map>
#include <pspkernel.h>
#include <pspgum.h>
#include <pspgu.h>

//disable to skip buffer verification (slightly faster loading)
#define VERIFY_FBUFS false

Scene::Scene(std::string fname):
	source(fname),
	scene_data(nullptr),
	alloc(malloc),
	release(free)
{
	//set source_root to the containing folder
	//e.g. "MAIN/warehouse.gltf"
	for(int i = source.size() - 1; i >= 0; i--){
		if(source[i] == '/'){
			source_root = source.substr(0, i);
			break;
		}
	}

	printf("Scene::Scene(%s), source_root: %s\n", source.c_str(), source_root.c_str());
}

Scene::~Scene()
{
	unloadAll();
}

void Scene::unloadAll(){
	unloadMeshes();
	unloadScene();
}

void Scene::unloadMeshes(){
	for(int i = 0; i < mesh_map.size(); i++){
		release(mesh_map[i]);
	}
	mesh_map.clear();
	bboxes.clear();
}
void Scene::unloadScene(){
	if(scene_data != nullptr){
		release(scene_data);
		scene_data = nullptr;
	}
}

bool Scene::loadAll(bool calc_bb){
	if(loadScene()){
		if(loadMeshes(calc_bb)){
			return true;
		}
	}
	return false;
}

int get_filesize(FILE *file){
	int msize = -1;
	if(fseek(file, 0, SEEK_END)){
		printf("fseek failed!\n");
		return -1;
	}
	msize = ftell(file);
	if(msize == -1){
		printf("ftell failed!\n");
		return -1;
	}
	if(fseek(file, 0, SEEK_SET)){
		printf("2nd fseek failed!!\n");
		return -1;
	}
	return msize;
}

bool Scene::loadScene(){
	if(scene_data != nullptr){
		return true;
	}
	FILE *file = fopen(source.c_str(), "rb");
	if(file == NULL){
		printf("Failed to open file \"%s\"\n", source.c_str());
		return false;
	}

	int msize = get_filesize(file);	
	if(msize < 0){
		printf("Failed to read size of file \"%s\"\n", source.c_str());
		return false;
	}
	scene_data = (uint8_t*) alloc(msize);

	int r = fread((void*)scene_data, 1, msize, file);
	if(r != msize){
		printf("Error while reading scene \"%s\". (short read: %d / %d) Aborting!\n", source.c_str(), r, msize);
		return false;
	}

	#if VERIFY_FBUFS
	printf("Verifying PSPScene buffer (%d bytes)...\n", msize);
	auto verifier = flatbuffers::Verifier(scene_data, msize);
	if(!Engine::Graphics::VerifyPSPSceneBuffer(verifier)){
		printf("Failed to verify PSPScene file! It might be corrupt.\n");
		return false;
	}else{
		printf("\tOK!\n");
	}
	#endif

	fclose(file);

	return true;
}

static void calculate_bb(BoundingBox &out, const Engine::Graphics::PSPMesh *mesh){
	float min[3] = {FLT_MAX, FLT_MAX, FLT_MAX};
	float max[3] = {-FLT_MAX, -FLT_MAX, -FLT_MAX};

	for(int p = 0; p < mesh->parts()->size(); p++){
		auto part = mesh->parts()->Get(p);
		assert(part->vertex_format() & GU_VERTEX_32BITF && "Unsupported vertex format!");
		size_t stride = part->vertex_stride();
		uint8_t *pVert = (uint8_t *) part->vertices()->data();
		uint8_t *end = (uint8_t *) part->vertices()->data() + part->vertices()->size();
		for(int i = 0; pVert < end; pVert += stride, i++){
			float x = *reinterpret_cast<const float*>((pVert + stride) - (sizeof(float) * 3));
			float y = *reinterpret_cast<const float*>((pVert + stride) - (sizeof(float) * 2));
			float z = *reinterpret_cast<const float*>((pVert + stride) - (sizeof(float) * 1));
			if(x < min[0]) min[0] = x;
			if(y < min[1]) min[1] = y;
			if(z < min[2]) min[2] = z;

			if(x > max[0]) max[0] = x;
			if(y > max[1]) max[1] = y;
			if(z > max[2]) max[2] = z;
		}
	}

	out.verts[0].x = min[0];
	out.verts[0].y = min[1];
	out.verts[0].z = min[2];
	
	out.verts[1].x = min[0];
	out.verts[1].y = min[1];
	out.verts[1].z = max[2];
	
	out.verts[2].x = min[0];
	out.verts[2].y = max[1];
	out.verts[2].z = min[2];
	
	out.verts[3].x = min[0];
	out.verts[3].y = max[1];
	out.verts[3].z = max[2];

	out.verts[4].x = max[0];
	out.verts[4].y = min[1];
	out.verts[4].z = min[2];
	
	out.verts[5].x = max[0];
	out.verts[5].y = min[1];
	out.verts[5].z = max[2];
	
	out.verts[6].x = max[0];
	out.verts[6].y = max[1];
	out.verts[6].z = min[2];
	
	out.verts[7].x = max[0];
	out.verts[7].y = max[1];
	out.verts[7].z = max[2];
	
	assert(((size_t)(&out.verts[0].x)) % 16 == 0);
}

bool Scene::loadMeshes(bool calc_bb){
	if(scene_data == nullptr){
		return false;
	}
	if(mesh_map.size() != 0){
		return true;
	}
	assert(!calc_bb || (bboxes.size() == 0 && calc_bb) && "Didn't clear bounding box cache");

	auto scene = getScene();
	mesh_map.resize(scene->meshes()->size());

	if(calc_bb){
		bboxes.resize(scene->meshes()->size());
	}

	std::stringstream ss;
	std::string fname_str;

	std::map<std::string, uint64_t> load_times;

	for(int i = 0, ct = scene->meshes()->size(); i < ct; i++){
		std::string mname = scene->meshes()->Get(i)->c_str();
		uint64_t t_start = sceKernelGetSystemTimeWide();

		ss.str(std::string());
		
		assert(i < scene->meshes()->size());

		ss << source_root << "/" << scene->meshes()->Get(i)->c_str() << ".pspmesh";
		fname_str = ss.str();

		FILE *file = fopen(fname_str.c_str(), "rb");
		if(file == NULL){
			printf("\tFailed to open scene mesh \"%s\". Aborting!\n", fname_str.c_str());
			return false;
		}

		int msize = get_filesize(file);	
		if(msize < 0){
			printf("\tFailed to read size of file \"%s\"\n", fname_str.c_str());
			return false;
		}

		mesh_map[i] = (uint8_t*)alloc(msize);
		if(mesh_map[i] == nullptr){
			printf("\tFailed to allocate %u bytes for mesh %s. Aborting!\n", msize, fname_str.c_str());
			return false;
		}

		int r = fread((void*)mesh_map[i], 1, msize, file);
		if(r != msize){
			printf("\tError while reading scene mesh \"%s\". (short read: %d / %d) Aborting!\n", fname_str.c_str(), r, msize);
			return false;
		}

		#if VERIFY_FBUFS
		printf("\tverifying PSPMesh (%d bytes)...\n", msize);
		auto verifier = flatbuffers::Verifier(mesh_map[i], msize);
		if(!Engine::Graphics::VerifyPSPMeshBuffer(verifier)){
			printf("Failed to verify PSPMesh file! It might be corrupt.\n");
			return false;
		}else{
			printf("\tOK!\n");
		}
		#endif

		if(calc_bb){
			calculate_bb(bboxes[i], Engine::Graphics::GetPSPMesh(mesh_map[i]));
		}

		fclose(file);
		load_times[mname] = (sceKernelGetSystemTimeWide() - t_start) / 1000;
	}

	printf("Mesh load times:\n");
	uint64_t total = 0;
	for (auto const& [mesh, time]: load_times){
		printf("\t%s: %" PRIu64 " ms\n", mesh.c_str(), time);
		total += time;
	}
	printf("\t[TOTAL: %" PRIu64 " ms]\n", total);

	return true;
}

const Engine::Graphics::PSPMesh *Scene::getMesh(int idx){
	if(mesh_map[idx] == nullptr){ //incase a mesh is individually unloaded
		return nullptr;
	}
	return Engine::Graphics::GetPSPMesh(mesh_map[idx]);
}
const Engine::Graphics::PSPScene *Scene::getScene(){
	if(scene_data == nullptr){
		return nullptr;
	}
	return Engine::Graphics::GetPSPScene(scene_data);
}

void Scene::activateCamera(int idx, float fov_override, float aspx_override){
	auto scene = getScene();

	assert(idx < scene->cameras()->size() && "Camera out of bounds");

	auto camera = scene->cameras()->Get(idx);
	auto transform = camera->trans();
	
	sceGumMatrixMode(GU_PROJECTION);
	sceGumLoadIdentity();

	float yfov = camera->yfov();
	float aspx = camera->aspect_ratio();

	if(fov_override > 0.0f){
		yfov = fov_override;
	}

	if(aspx_override > 0.0f){
		aspx = aspx_override;
	}
	
	sceGumPerspective(
		yfov,
		aspx,
		camera->znear(),
		camera->zfar()
	);

	sceGumMatrixMode(GU_VIEW);
	sceGumLoadIdentity();

	ScePspFVector3 translation = {
		-transform->translation().x(),
		-transform->translation().y(),
		-transform->translation().z()
	};
	ScePspFVector3 rotation = {
		-transform->rotation().x(),
		-transform->rotation().y(),
		-transform->rotation().z()
	};

	sceGumRotateXYZ(&rotation);
	sceGumTranslate(&translation);
}

void Scene::drawAllMeshes(){
	auto scene = getScene();
	bool bbox = bboxes.size() > 0;

	for(int i = 0, ct = scene->mesh_nodes()->size(); i < ct; i++){
		auto node = scene->mesh_nodes()->Get(i);
		int mesh_idx = node->mesh();
		auto mesh = getMesh(mesh_idx);
		auto transform = node->trans();

		sceGumMatrixMode(GU_MODEL);
		sceGumLoadIdentity();

		sceGumTranslate(reinterpret_cast<const ScePspFVector3*>(&transform->translation()));
		sceGumRotateXYZ(reinterpret_cast<const ScePspFVector3*>(&transform->rotation()));
		sceGumScale(reinterpret_cast<const ScePspFVector3*>(&transform->scale()));
		sceGumUpdateMatrix();

		if(bbox){
			float *bbvec = &bboxes[mesh_idx].verts[0].x;
			assert(((size_t)bbvec) % 16 == 0);
			sceGuBeginObject(GU_VERTEX_32BITF|GU_TRANSFORM_3D, 8, 0, bbvec);
		}

		for(int v = 0; v < mesh->parts()->size(); v++){
			auto part = mesh->parts()->Get(v);
			sceGuDrawArray(
				part->primitive(),
				part->vertex_format(),
				part->num_elements(),
				part->indices()->data(),
				part->vertices()->data()
			);
		}

		if(bbox){
			sceGuEndObject();
		}
	}
}

const Engine::Graphics::PSPCameraNode *Scene::getCamera(int idx){
	auto scene = getScene();
	return scene->cameras()->Get(idx);
}

int Scene::findCamera(std::string name){
	auto scene = getScene();
	for(int i = 0, ct = scene->cameras()->size(); i < ct; i++){
		auto cam = scene->cameras()->Get(i);
		if(name == cam->name()->c_str()){
			return i;
		}
	}
}

const Engine::Graphics::PSPSceneNode *Scene::getEmpty(int idx){
	auto scene = getScene();
	return scene->empty_nodes()->Get(idx);
}

int Scene::findEmpty(std::string name){
	auto scene = getScene();
	for(int i = 0, ct = scene->empty_nodes()->size(); i < ct; i++){
		auto node = scene->empty_nodes()->Get(i);
		if(name == node->name()->c_str()){
			return i;
		}
	}
	printf("Failed to find empty with the name \"%s\"\n", name.c_str());
	return -1;
}

const Engine::Graphics::PSPMeshNode *Scene::getMeshNode(int idx){
	auto scene = getScene();
	return scene->mesh_nodes()->Get(idx);
}

int Scene::findMeshNode(std::string name){
	auto scene = getScene();
	for(int i = 0, ct = scene->mesh_nodes()->size(); i < ct; i++){
		auto node = scene->mesh_nodes()->Get(i);
		if(name == node->name()->c_str()){
			return i;
		}
	}
	printf("Failed to find mesh node with the name \"%s\"\n", name.c_str());
	return -1;
}