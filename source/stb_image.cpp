/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#define STBI_NO_THREAD_LOCALS //'prx unsupported error' on device without this
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"