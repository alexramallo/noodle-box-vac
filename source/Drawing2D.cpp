/*
	Copyright (c) 2022 Alejandro Ramallo

	This file is part of VAC Demo
	Licensed under the BSD license, see the file "COPYING" for details.
*/
#include <vector>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "Game.h"
#include "Drawing2D.h"
#include "FontBuilderImporter.h" 

#include <pspgu.h>
#include <pspkernel.h>
#include <pspge.h>
#include <pspgum.h>

#include <stb_image.h>
#include <limits>

extern void* (*alloc_texture)(size_t);
extern void (*release_texture)(void*);

using namespace FontBuilder;

template<typename T>
constexpr T calc_alignment(T value, T alignment){
	T m = value % alignment;
	if(m){
		value += alignment - m;
	}
	return value;
}
extern int get_filesize(FILE *file);

namespace Drawing2D {

	struct Vert2D {
		float u, v;
		uint16_t C4444;
		float x, y, z;

		static const unsigned int Format = GU_TEXTURE_32BITF|GU_COLOR_4444|GU_VERTEX_32BITF|GU_TRANSFORM_3D;

		Vert2D():
			u(0), v(0),
			C4444(0),
			x(0), y(0), z(0)
		{
			//--
		}

		Vert2D(float X, float Y, Color col):
			u(0), v(0),
			C4444(col),
			x(X), y(Y), z(0)
		{
			//--
		}

		Vert2D(float X, float Y, float U, float V):
			u(U), v(V),
			C4444(0xFFFFFFFF),
			x(X), y(Y), z(0)
		{
			//--
		}

		Vert2D(float X, float Y, Color col, float U, float V):
			u(U), v(V),
			C4444(col),
			x(X), y(Y), z(0)
		{
			//--
		}
	};
	
	Vert2D *verts = nullptr;
	size_t vertbuf_len = 0;
	size_t max_verts = 0;
	int vert_head = 0;

	static Vert2D *get_verts(int count){
		if(vert_head + count > max_verts){
			return nullptr;
		}
		Vert2D *ret = &verts[vert_head];
		vert_head += count;
		return ret;
	}

	void InitDrawing(uint8_t *vertbuf, size_t len){
		verts = (Vert2D*) vertbuf;
		vertbuf_len = len;
		max_verts = len / sizeof(Vert2D);
	}

	void BeginDrawing(){	
		sceGumMatrixMode(GU_PROJECTION);
		sceGumLoadIdentity();
		sceGumOrtho(0, SCR_WIDTH, SCR_HEIGHT, 0, -1, 1);

		sceGumMatrixMode(GU_VIEW);
		sceGumLoadIdentity();
		sceGumMatrixMode(GU_MODEL);
		sceGumLoadIdentity();
		sceGumMatrixMode(GU_TEXTURE);
		sceGumLoadIdentity();
		
		sceGuDisable(GU_DEPTH_TEST);
		sceGuEnable(GU_ALPHA_TEST);

		sceGuTexMode(GU_PSM_8888,0,0,0);
		sceGuAlphaFunc(GU_GREATER, 0, 0xff);
		sceGuTexFunc(GU_TFX_MODULATE,GU_TCC_RGBA);
		SetImageFilter(false);
		sceGuTexScale(1.0f,1.0f);
		sceGuTexOffset(0.0f,0.0f);
	}
	
	void SetImageFilter(bool linear){
		if(linear){
			sceGuTexFilter(GU_LINEAR, GU_LINEAR);				
		}else{
			sceGuTexFilter(GU_NEAREST, GU_NEAREST);
		}
	}

	void EndDrawing(bool reset){
		if(reset){
			vert_head = 0;
		}
		sceGuDisable(GU_ALPHA_TEST);
		sceGuEnable(GU_DEPTH_TEST);
	}

	Color RGBA8(uint8_t r, uint8_t g, uint8_t b, uint8_t a){
		// return 	(Color)
		// (((uint8_t)a) << 24) |
		// (((uint8_t)b) << 16) |
		// (((uint8_t)g) << 8) |
		// ((uint8_t)r);
		return (Color)
			(uint8_t)(15.0f * (((float)a) / 255.0f)) << 12 |
			(uint8_t)(15.0f * (((float)b) / 255.0f)) << 8 |
			(uint8_t)(15.0f * (((float)g) / 255.0f)) << 4 |
			(uint8_t)(15.0f * (((float)r) / 255.0f));
	}

	void freeImage(Image &img){
		if(img.data != nullptr){
			release_texture(img.data);
			img.data = nullptr;
		}
	}

	Font loadFontMemory(uint8_t *buffer, size_t buflen, int ptsize){
		Font ret {nullptr, 0};

		FontData *data = FontBuilder::importFontDataEmbedded(buffer, buflen);
		if(data == nullptr){
			printf("Failed to import FontBuilder data\n");
			return ret;
		}

		//Load atlas
		ret.atlas = loadImage(data->filename.c_str());

		ret.data = data;
		ret.ptsize = ptsize;

		return ret;
	}

	Font loadFont(const char *path, int ptsize) {
		Font ret {nullptr, 0};

		FILE *fontfile = fopen(path, "rb");
		if(fontfile == nullptr){
			printf("Failed to load font \"%s\"\n", path);
			return ret;
		}
		
		int buflen = get_filesize(fontfile);
		uint8_t *fontdef = (uint8_t*) malloc(buflen);
		if(fontdef == nullptr){
			printf("Failed to allocate %u bytes while loading fontdef\n", buflen);
			return ret;
		}
		int fontdef_read;
		if((fontdef_read = fread(fontdef, 1, buflen, fontfile)) != buflen){
			printf("Error while reading fontdef (short read: %u / %u)\n", fontdef_read, buflen);
			return ret;
		}

		ret = loadFontMemory(fontdef, buflen, ptsize);
		free(fontdef);
		fclose(fontfile);
		return ret;
	}

	void freeFont(Font &font){
		if(font.data != nullptr){
			FontBuilder::freeFontData((FontData*)font.data);
			font.data = nullptr;
			font.ptsize = 0;
		}
	}

	void DrawRectangle(int x, int y, int w, int h, Color color){
		Vert2D *verts = get_verts(2);
		assert(verts != nullptr);
		assert(((size_t)verts) % 16 == 0);
		//Vert2D verts[2];
		new(&verts[0]) Vert2D(
			x,
			y,
			color
		);

		new(&verts[1]) Vert2D(
			x + w,
			y + h,
			color
		);

		sceGuDisable(GU_TEXTURE_2D);
		sceGumDrawArray(
			GU_SPRITES,
			Vert2D::Format,
			2,
			nullptr,
			(void*)&verts[0]
		);
	}

	void DrawText(const Font &font, const char *text, float x, float y, Color color, float scale_x, float scale_y){
		assert(font.atlas.height != 0);
		assert(font.atlas.width != 0);

		int num_chars = strlen(text);
		const FontData *data = (FontData*) font.data;
		Vert2D *verts = get_verts(num_chars * 2);

		assert(verts != nullptr);
		assert(data != nullptr);

		float tx = 0.0f;
		float ty = 0.0f;

		float aw = font.atlas.width;
		float ah = font.atlas.height;

		int a = 0;
		int b = 0;
		int LH = data->lineHeight;
		for(int i = 0; i < num_chars; i++){
			int gID = (int) text[i];

			const FontBuilder::GlyphData &glyph = data->glyphs[gID];

			float xx = floor(tx + (glyph.OffsetX + a));
			float yy = floor(ty + (b + glyph.OffsetY - LH));

			new(&verts[(i*2)+0]) Vert2D(
				xx,
				yy,
				color,
				(float)glyph.PlaceX / aw,
				(float)glyph.PlaceY / ah
			);
			new(&verts[(i*2)+1]) Vert2D(
				xx + glyph.PlaceW,
				yy + glyph.PlaceH,
				color,
				(float)(glyph.PlaceX + glyph.PlaceW) / aw,
				(float)(glyph.PlaceY + glyph.PlaceH) / ah
			);

			a += glyph.AdvanceX;
		}

		sceGuEnable(GU_TEXTURE_2D);
		sceGuTexImage(0, font.atlas.width, font.atlas.height, font.atlas.stride, font.atlas.data);

		sceGumMatrixMode(GU_MODEL);
		sceGumPushMatrix();

		ScePspFVector3 tr = {
			x, y, 0.0f
		};
		ScePspFVector3 sc = {
			scale_x, scale_y, 1.0f
		};
		sceGumTranslate(&tr);
		sceGumScale(&sc);
		
		sceGumDrawArray(
			GU_SPRITES,
			Vert2D::Format,
			num_chars * 2,
			nullptr,
			(void*)&verts[0]
		);
		sceGumPopMatrix();
	}

	void DrawLine(int x0, int y0, int x1, int y1, Color color){
		Vert2D *verts = get_verts(2);
		assert(verts != nullptr);
		assert(((size_t)verts) % 16 == 0);
		//Vert2D verts[2];
		new(&verts[0]) Vert2D(
			x0,
			y0,
			color
		);

		new(&verts[1]) Vert2D(
			x1,
			y1,
			color
		);

		sceGuDisable(GU_TEXTURE_2D);
		sceGumDrawArray(
			GU_LINES,
			Vert2D::Format,
			2,
			nullptr,
			(void*)&verts[0]
		);
	}

	void DrawImage(const Image &tex, float x, float y, float tx, float ty, float tw, float th, float x_scale, float y_scale, float angle, Color tint){
		float tx2 = tx + tw;
		float ty2 = ty + th;
		float ww2 = (tex.width * tw) / 2.0f;
		float hh2 = (tex.height * th) / 2.0f;

		Vert2D *verts = get_verts(2);
		new(&verts[0]) Vert2D(
			-ww2,
			-hh2,
			tint,
			tx,
			ty
		);

		new(&verts[1]) Vert2D(
			ww2,
			hh2,
			tint,
			tx2,
			ty2
		);

		sceGuEnable(GU_TEXTURE_2D);
		sceGuTexImage(0, tex.width, tex.height, tex.stride, tex.data);

		sceGumMatrixMode(GU_MODEL);
		sceGumPushMatrix();

		ScePspFVector3 tr = {
			x+(ww2 * x_scale), y+(hh2 * y_scale), 0.0f
		};
		ScePspFVector3 sc = {
			x_scale, y_scale, 1.0f
		};
		sceGumTranslate(&tr);	
		sceGumRotateZ(angle * GU_PI / 180.0f);
		sceGumScale(&sc);
		
		sceGumDrawArray(
			GU_SPRITES,
			Vert2D::Format,
			2,
			nullptr,
			(void*)&verts[0]
		);
		sceGumPopMatrix();
	}

	Image loadImage(const char *path){
		Image ret;
		ret.data = nullptr;
		ret.ok = false;

		FILE *file = fopen(path, "rb");
		if(file == nullptr){
			printf("Failed to open image \"%s\"\n", path);
			return ret;
		}

		int img_size = get_filesize(file);
		uint8_t *img_data = (uint8_t*) malloc(img_size);
		if(img_data == nullptr){
			printf("Failed to malloc %u bytes while loading image\n", img_size);
			fclose(file);
			return ret;
		}

		if(fread(img_data, 1, img_size, file) != img_size){
			printf("Short read while loading image \"%s\" (expected: %u)\n", path, img_size);
			free(img_data);
			fclose(file);
			return ret;
		}

		fclose(file);

		int width, height, channels;
		uint8_t *pixels = (uint8_t*)stbi_load_from_memory(img_data, img_size, &width, &height, &channels, 4);
		channels = 4;

		if(pixels == nullptr){
			printf("stbi_load_from_memory failed while loading image \"%s\"!\n", path);
			free(img_data);
			return ret;
		}

		size_t pixels_size = width * height * channels;

		uint8_t *tex_mem = (uint8_t*)alloc_texture(pixels_size);
		if(tex_mem == nullptr){
			printf("Failed to allocate %u bytes of texture data while loading image\n", pixels_size);
			return ret;
		}
		assert(((size_t)tex_mem) % 16 == 0);
		memcpy(tex_mem, pixels, pixels_size);

		stbi_image_free(pixels);
		free(img_data);

		ret.ok = true;
		ret.data = tex_mem;
		ret.width = width;
		ret.height = height;
		
		ret.stride = 1;
		while(ret.stride < width){
			ret.stride *= 2;
		}

		printf("Loaded image \"%s\" successfully!\n", path);

		return ret;
	}

}; //namespace Drawing
